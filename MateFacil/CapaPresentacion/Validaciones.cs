﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Excel;
namespace CapaNegocio
{
    class Validaciones
    {
        //valirdar correo
        public static bool banderaPunto = true;
        public static bool banderaEspacio = true;
        public static bool banderaCadena = true;
        public Boolean ValirCorreo(String e)
        {
            return Regex.IsMatch(e, "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        }
        public Boolean ValidarNombreAlumno(String e)
        {
            return Regex.IsMatch(e, "^(\\.)?");
        }
        public static void ValidaAgregarAlumno(KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 65 && e.KeyChar <= 90) && banderaCadena == true || (e.KeyChar >= 97 && e.KeyChar <= 122) && banderaCadena == true)
            {
                e.Handled = false;
                banderaPunto = false;
                banderaEspacio = false;
            }
            else if (e.KeyChar == '.' && banderaPunto == false)
            {
                e.Handled = false;
                banderaPunto = true;
                banderaEspacio = false;
                banderaCadena = false;
            }
            else if (e.KeyChar == '.' && banderaPunto == true)
            {
                e.Handled = true;
            }
            else if (e.KeyChar == 32 && banderaEspacio == false)
            {
                e.Handled = false;
                banderaEspacio = true;
                banderaCadena = true;
            }
            else if (e.KeyChar == 32 && banderaEspacio == false)
            {
                e.Handled = true;
            }
            else if (e.KeyChar == 08)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        public static void ValidaGrupoAlumno(KeyPressEventArgs e)
        {
            if(e.KeyChar >= 48 && e.KeyChar <= 57)
            {
                e.Handled = false;
            }
            else if(e.KeyChar == 08)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        public static void ValidarNombreUsuario(KeyPressEventArgs e)
        {
            //Valida si el valor ingresado esta en el rango [a-z]-[A-Z] 
            if ((e.KeyChar >= 65 && e.KeyChar <= 90) || (e.KeyChar >= 97 && e.KeyChar <= 122)) 
            {
                e.Handled = false; //Permite el valor
                banderaPunto = false; //Desactiva la BanderaPunto
                banderaEspacio = false; //Desactiva la banderaEspacio
            }
            //Valida si el valor ingreasado esta en el rango [0-9]
            else if (e.KeyChar >= 48 && e.KeyChar <= 57)
            {
                e.Handled = false; //Permite el valor
                banderaPunto = false; //Se desactiva la banderaPunto
            }
            //Valida si el valor ingresado es un [.] y la banderaPunto esta desactivada
            else if (e.KeyChar == '.' && banderaPunto == false)
            {
                e.Handled = false; //Permite el valor
                banderaPunto = true; //Activa la bandePunto
                banderaEspacio = false; //Desactiva la banderaEspacio
            }
            //Valida si el valor ingresado es un [.] y la banderaPunto esta activada
            else if (e.KeyChar == '.' && banderaPunto == true)
            {
                e.Handled = true; //No permite el valor
            }
            //Valida si fue un retroceso
            else if (e.KeyChar == 08)
            {
                e.Handled = false;//Permite el valor
            }
            else
            {
                e.Handled = true; //No permite los demas valores
            }
        }
        public static void ArrobaYPunto(KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar) || Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar) || Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("@"))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }
        public static void LetrasYNumeros(KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar) || Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar) || Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }
        public static void LetrasYPunto(KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar) || Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
                banderaPunto = false;
            }
            else if (Char.IsSeparator(e.KeyChar) || Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
                banderaPunto = false;
            }
            else if (char.IsControl(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
                banderaPunto = false;
            }
            else if (e.KeyChar == '.' && banderaPunto == false)
            {
                banderaPunto = true;
                e.Handled = false;
            }
            else if(e.KeyChar == '.' && banderaPunto == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }

        }
        public static void SoloLetras(KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }


        }
        public static void SoloNumeros(KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        public static void NumeroDecimal(KeyPressEventArgs e,String a)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                Boolean b = false;
                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i].ToString().Equals("."))
                    {
                        b = true;
                    }
                }

                if (b == false)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
                
            }
            else
            {
                e.Handled = true;
            }
        }

        public void ExportarDataGridViewExcel(DataGridView grd)
        {
            try
            {

                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                fichero.FileName = "ArchivoExportado";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
                    Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;

                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

                    //Recorremos el DataGridView rellenando la hoja de trabajo
                    for (int i = 0; i < grd.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            if ((grd.Rows[i].Cells[j].Value == null) == false)
                            {
                                hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                            }
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName,Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    MessageBox.Show("Tabla exportada a excel");
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al exportar la informacion debido a: " + ex.ToString());
            }

        }
    }
}
