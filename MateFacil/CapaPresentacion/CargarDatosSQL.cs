﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using System.Windows.Forms;

using CapaDatos;


namespace CapaDatos
{
    class CargarDatosSQL
    {
        CDConexion conexion = new CDConexion();
        DataTable dt;
        DataTable gp;
        SqlDataAdapter da;
        SqlDataAdapter gt;
        public void CargarGrupos(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select * from Datos_Grupo", conexion.AbrirConexion());
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errorm dataGridview: " + ex.ToString());
            }
        }
        public void CargarAlumnos(DataGridView dgv,String curso)
        {
            try
            {
                da = new SqlDataAdapter("listar_registros '" + curso + "'", conexion.AbrirConexion());
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errorm dataGridview: " + ex.ToString());
            }
        }

        public void CargarEjercicios(DataGridView dgv,String cadena,String curso)
        {
            try
            {
                da = new SqlDataAdapter("DesplegarEjercicios '"+cadena+"','"+curso+"'", conexion.AbrirConexion());
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errorm dataGridview: " + ex.ToString());
            }
        }

        public void CargarEjercicios2(DataGridView dgv, String idalumno, String idleccion)
        {
            try
            {
                da = new SqlDataAdapter("DesplegarEjerciciosAlumno '" + idalumno + "','" + idleccion + "'", conexion.AbrirConexion());
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errorm dataGridview: " + ex.ToString());
            }
        }

        public void CargarGrupos(DataGridView dgv, String cadena)
        {
            try
            {
                gt = new SqlDataAdapter("DezplegarGrupos'" + cadena + "'", conexion.AbrirConexion());
                gp = new DataTable();
                gt.Fill(dt);
                dgv.DataSource = gp;        
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error datGridView: "+ex.ToString());
            }
        }

        
    }
}
