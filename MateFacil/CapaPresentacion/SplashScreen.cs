﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            MateFacil.IngresarProfesor frm = new MateFacil.IngresarProfesor();
            bunifuProgressBar2.Value += 1;
            if (bunifuProgressBar2.Value == 100)
            {
                timer1.Stop();
                frm.Show();
                this.Hide();
            }
        }
    }
}
