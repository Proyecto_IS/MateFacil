﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Cargar : Form
    {
        public Cargar()
        {
            InitializeComponent();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            for(int i = 0; i <= 100; i++)
            {
                Thread.Sleep(50);
                circulo.Value = i;
                circulo.Update();
            }
            timer1.Stop();
            MateFacil.Form1 nuevo = new MateFacil.Form1();
            nuevo.Show();
            this.Hide();
        }

        private void Cargar_Load(object sender, EventArgs e)
        {
            circulo.Value = 0;
            circulo.Minimum = 0;
            circulo.Maximum = 100;
        }
    }
}
