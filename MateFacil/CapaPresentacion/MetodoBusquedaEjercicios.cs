﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using CapaDatos;

namespace CapaPresentacion
{
    class MetodoBusquedaEjercicios
    {
        SqlDataReader dr;
        CDConexion conexion = new CDConexion();
        public void Listar(DataGridView data)
        {
            try
            {
                SqlDataAdapter de = new SqlDataAdapter("listar_ejercicios", conexion.AbrirConexion());
                de.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                de.Fill(dt);
                data.DataSource = dt;

                data.Columns[0].Width = 100;
                data.Columns[0].HeaderCell.Value = "Id";
                data.Columns[1].Width = 300;
                data.Columns[1].HeaderCell.Value = "Planteamiento";
                data.Columns[2].Width = 100;
                data.Columns[2].HeaderCell.Value = "Respuesta";
                data.Columns[3].Width = 100;
                data.Columns[3].HeaderCell.Value = "Leccion";

                data.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conexion.CerrarConexion();
            }
        }
       

        public void autoCompletar(TextBox cajaTexto, String cadena)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Select Nombre_Alumno from Alumno where Id_Curs= '"+cadena+"'", conexion.AbrirConexion());
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cajaTexto.AutoCompleteCustomSource.Add(dr["Nombre_Alumno"].ToString());
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo autocompletar el TextBox:" + ex.ToString());
            }
        }
    }
}
