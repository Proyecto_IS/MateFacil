﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using CapaDatos;
namespace MateFacil
{
    class MetodoBusqueda
    {
        SqlDataReader dr;
        CDConexion conexion = new CDConexion();
        public void Listar(DataGridView data,String curso)
        {
            try
            {
                SqlCommand da = new SqlCommand("listar_registros ", conexion.AbrirConexion());
                da.CommandType = CommandType.StoredProcedure;
                da.Parameters.Add("@id_curso", SqlDbType.VarChar, 5).Value = curso;
                da.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter de = new SqlDataAdapter(da);
                de.Fill(dt);
                data.DataSource = dt;

                data.Columns[0].Width = 100;
                data.Columns[0].HeaderCell.Value = "Id"; //Id_Alumno
                data.Columns[1].Width = 300;
                data.Columns[1].HeaderCell.Value = "Alumno"; //Nombre_Alumno
                data.Columns[2].Width = 100;
                data.Columns[2].HeaderCell.Value = "Grado"; //Grado
                data.Columns[3].Width = 100;
                data.Columns[3].HeaderCell.Value = "Grupo"; //Id_Grupo
               // data.Columns[4].Width = 100;
                //data.Columns[4].HeaderCell.Value = "Curso"; //Id_Curso
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conexion.CerrarConexion();
            }
        }
        public void Filtrar(DataGridView data, string buscarnombre,string curso)
        {
            try
            {
                SqlCommand sql = new SqlCommand("Filtro_BusquedaDos", conexion.AbrirConexion());
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@filtro", SqlDbType.VarChar, 200).Value = buscarnombre;
                sql.Parameters.Add("@curso", SqlDbType.VarChar, 5).Value = curso;
                sql.ExecuteNonQuery();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(sql);
                da.Fill(dt);
                data.DataSource = dt;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conexion.CerrarConexion();
            }
        }
    }
}
