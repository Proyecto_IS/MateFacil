﻿namespace CapaPresentacion
{
    partial class Cargar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.circulo = new CircularProgressBar.CircularProgressBar();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(143, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cargando DB...";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // circulo
            // 
            this.circulo.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circulo.AnimationSpeed = 500;
            this.circulo.BackColor = System.Drawing.Color.Transparent;
            this.circulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold);
            this.circulo.ForeColor = System.Drawing.Color.Transparent;
            this.circulo.InnerColor = System.Drawing.Color.Transparent;
            this.circulo.InnerMargin = 2;
            this.circulo.InnerWidth = -1;
            this.circulo.Location = new System.Drawing.Point(12, 12);
            this.circulo.MarqueeAnimationSpeed = 2000;
            this.circulo.Name = "circulo";
            this.circulo.OuterColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.circulo.OuterMargin = -25;
            this.circulo.OuterWidth = 30;
            this.circulo.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.circulo.ProgressWidth = 15;
            this.circulo.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.circulo.Size = new System.Drawing.Size(125, 118);
            this.circulo.StartAngle = 270;
            this.circulo.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circulo.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circulo.SubscriptText = "";
            this.circulo.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circulo.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circulo.SuperscriptText = "";
            this.circulo.TabIndex = 3;
            this.circulo.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circulo.Value = 35;
            // 
            // Cargar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(333, 142);
            this.Controls.Add(this.circulo);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Cargar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cargar";
            this.Load += new System.EventHandler(this.Cargar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private CircularProgressBar.CircularProgressBar circulo;
    }
}