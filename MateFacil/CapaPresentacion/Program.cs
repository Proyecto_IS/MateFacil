﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaPresentacion;
using CapaPresentacion.InterfacesAlumno;
using CapaPresentacion.InterfacesProfesor;


namespace MateFacil
{
    static class Program
    {
        public static string curso="";
        public static Boolean abrir = false;
        public static string alu = "";
        public static Boolean bandiagnos = false;
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new IngresarProfesor());
        }
    }
}
