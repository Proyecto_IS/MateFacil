﻿using CapaDatos;
using CapaNegocio;
using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Ejercicios2 : Form
    {
        public Ejercicios2()
        {
            InitializeComponent();
        }

        CDConexion conexion = new CDConexion();
        CargarDatosSQL cargar = new CargarDatosSQL();
        public String alu = Program.alu;

        public int puntaje = 0;
        String idavance = "";



        public void MostrarEjercicios()
        {
            CNEjercicios objeto = new CNEjercicios();
            dgvEjercicios.DataSource = objeto.MostrarEjerciciosAlumnos(alu, "L02");
        }

        public void GenerarCodigoAvane()
        {

            SqlCommand comando = new SqlCommand("SELECT MAX(Id_Avance) FROM Avance", conexion.AbrirConexion());
            idavance = Convert.ToString(comando.ExecuteScalar());

            if (idavance.Equals(""))
            {
                idavance = "R00001";
            }
            else
            {
                String nuevo = "";

                for (int i = 1; i < 6; i++)
                {
                    nuevo += idavance[i];
                }

                int num = Convert.ToInt32(nuevo);
                num++;
                nuevo = Convert.ToString(num);
                if (nuevo.Length == 1)
                {
                    idavance = "R0000" + nuevo;
                }
                else if (nuevo.Length == 2)
                {
                    idavance = "R000" + nuevo;
                }
                else if (nuevo.Length == 3)
                {
                    idavance = "R00" + nuevo;
                }
                else if (nuevo.Length == 4)
                {
                    idavance = "R0" + nuevo;
                }
                else if (nuevo.Length == 5)
                {
                    idavance = "R" + nuevo;
                }


            }


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String respuesta = "";
            String r = "";
            String compara = "";
            for (int i = 0; i < dgvEjercicios.RowCount; i++)
            {
                try
                {
                    respuesta = dgvEjercicios.Rows[i].Cells[0].Value.ToString();
                }
                catch (Exception ex) { respuesta = ""; }
                String Id = dgvEjercicios.Rows[i].Cells[1].Value.ToString();
                SqlCommand comando = new SqlCommand("select Respuesta from Ejercicios where Id_Ejercicios= '" + Id + "'", conexion.AbrirConexion());
                r = Convert.ToString(comando.ExecuteScalar());
                conexion.CerrarConexion();

                if (r.Equals(respuesta))
                {
                    puntaje++;
                }

            }
            if (dgvEjercicios.RowCount != 0)
            {

                if (puntaje < dgvEjercicios.RowCount * 0.60)
            {
                MessageBox.Show("El 60% de los ejercicios deben estar correctos, intentalo de nuevo.");
                puntaje = 0;
            }
            else
            {
                    SqlCommand comparar = new SqlCommand("select Id_Alumno from Avance where Id_Alumno= '" + alu + "' and Id_Leccion= 'L02'", conexion.AbrirConexion());
                    comparar.ExecuteNonQuery();
                    compara = Convert.ToString(comparar.ExecuteScalar());
                    if (compara.Equals(alu))
                    {
                        MessageBox.Show("Tu ya realizaste estos ejercicios previamente tus resultados no se almacenarán.");
                    }
                    else
                    {
                        MessageBox.Show("Felicidades. Respondiste correctamente " + puntaje + " ejercicio(s). Ahora puedes realizar el test de la presente lección.");
                        GenerarCodigoAvane();

                        SqlCommand comando2 = new SqlCommand("insert into Avance values ('" + idavance + "','" + alu + "','L02',null," + puntaje + ",null)", conexion.AbrirConexion());
                        comando2.ExecuteNonQuery();
                        puntaje = 0;
                        Leccion lec = new Leccion();
                        lec.Close();

                       

                    }

                    Program.bandiagnos = false;
                }
            }
            else
            {
                MessageBox.Show("No se han agregado ejercicios para esta lección.");
                Program.bandiagnos = false;
            }
        }

        private void dgvEjercicios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void Ejercicios2_Load(object sender, EventArgs e)
        {
            cargar.CargarEjercicios2(dgvEjercicios, alu, "L02");
            MostrarEjercicios();
            DataGridViewColumn dgc = new DataGridViewColumn();
            dgc.HeaderText = "Respuestas";
            dgc.CellTemplate = dgvEjercicios.Columns[0].CellTemplate;
            dgvEjercicios.Columns.Add(dgc);
        }

        private void dgvEjercicios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int pos = e.RowIndex;
            if (e.ColumnIndex == 1)
            {
                dgvEjercicios.ReadOnly = true;
            }
            if (e.ColumnIndex == 2)
            {
                dgvEjercicios.ReadOnly = true;
            }
            if (e.ColumnIndex == 0)
            {
                dgvEjercicios.ReadOnly = false;
            }
        }
    }
    
}
