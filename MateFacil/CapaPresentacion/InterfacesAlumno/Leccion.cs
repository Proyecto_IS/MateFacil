﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using CapaDatos;
using MateFacil;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Leccion : Form
    {
        CDConexion conexion = new CDConexion();
        PrincipalAlumno pa = new PrincipalAlumno();
        public String alu = Program.alu;
        public Leccion()
        {
            InitializeComponent();
        }

        public void AbrirFormInPanel(object Formhijo)
        {
            if (this.panel2.Controls.Count > 0)
                this.panel2.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panel2.Controls.Add(fh);
            this.panel2.Tag = fh;
           
            panel1.SendToBack();
            panel3.SendToBack();
            panel4.SendToBack();
            

            fh.Show();
           
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Click(object sender, EventArgs e)
        {
            //AbrirFormInPanel(new Leccion1());
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            
            VerificarDiagnostico("L02");
        }

        

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
          
            VerificarDiagnostico("L03");
        }

        private void bunifuFlatButton4_Click_1(object sender, EventArgs e)
        {
            VerificarDiagnostico("L01");
           
            
        }

        public void AbrirLeccion(String leccion)
        {
            String cad = "";
            SqlCommand comando = new SqlCommand("LeccionAlumno '" + alu + "','"+leccion+"'", conexion.AbrirConexion());
            cad = Convert.ToString(comando.ExecuteScalar());
            if (cad.Equals("L01"))
            {
                AbrirFormInPanel(new Leccion1());
            }
            else if (cad.Equals("L02"))
            {
                VerificarLeccion("L02");
            }
           else if (cad.Equals("L03"))
            {
                VerificarLeccion("L03");
            }
           else 
            {
                MessageBox.Show("Esta lección no esta disponible");
            }
            conexion.CerrarConexion();
        }

        public void VerificarLeccion(String leccion)
        {
            String cad = "";
            Boolean ban = false;
            String sentencia = "ObtenerLeccionesCurso '" + alu + "'";
            SqlCommand comando = new SqlCommand(sentencia, conexion.AbrirConexion());

            SqlDataReader reader = comando.ExecuteReader();

            List<String> resultado = new List<String>();
        
            while (reader.Read())
            {
                resultado.Add(Convert.ToString(reader["Id_Leccion"]));
                
            }

            conexion.CerrarConexion();

            SqlCommand comando2 = new SqlCommand("ObtenerUltimaLeccion '" + alu + "'", conexion.AbrirConexion());
            SqlDataReader reader2 = comando2.ExecuteReader();

            List<String> resultado2 = new List<String>();

            while (reader2.Read())
            {
                resultado2.Add(Convert.ToString(reader2["Id_Leccion"]));

            }

            conexion.CerrarConexion();
            if (leccion.Equals(resultado[0]) || leccion.Equals("L01"))
            {
                ban = true;
            }
            
            if (resultado2.Count > 0 && ban==false)
            {
                for (int i = 0; i < resultado2.Count; i++)
                {
                    if ((resultado[i].Equals(resultado2[i])) && (resultado[i+1].Equals(leccion)))
                    {
                        ban = true;
                        break;
                    }
                }
            }

            if (ban == true)
            {
                if (leccion.Equals("L02"))
                { AbrirFormInPanel(new Leccion2()); }
                if (leccion.Equals("L03"))
                { AbrirFormInPanel(new Leccion3()); }
            }
            else
            {
                MessageBox.Show("Debes completar la lección previa para ingresar.");
            }



          



        }

        public void VerificarDiagnostico(String id)
        {
            String compara = "";

            SqlCommand comparar = new SqlCommand("select Examen_D from Calificaciones where Id_Alumno= '" + alu + "'", conexion.AbrirConexion());
            comparar.ExecuteNonQuery();
            compara = Convert.ToString(comparar.ExecuteScalar());

            if (compara.Equals(""))
            {
                MessageBox.Show("Debes realizar el examen diagnóstico antes de ingresar a las lecciones."); 
            }
            else
            {
                AbrirLeccion(id);
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
