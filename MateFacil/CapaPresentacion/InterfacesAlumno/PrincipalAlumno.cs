﻿using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;
namespace CapaPresentacion.InterfacesAlumno
{
    public partial class PrincipalAlumno : Form
    {

       
        public PrincipalAlumno()
        {
            InitializeComponent();
        }

        CDConexion conexion = new CDConexion();
        public void AbrirFormInPanel(object Formhijo)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();
        }
        private void iconrestaurar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            iconrestaurar.Visible = false;
            iconmaximizar.Visible = true;
        }

        private void iconminimizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void iconmaximizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            iconrestaurar.Visible = true;
            iconmaximizar.Visible = false;
        }

        private void iconcerrar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        Point lastPoint;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void MenuVertical_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Menu_Click(object sender, EventArgs e)
        {
            if(MenuVertical.Width == 77)
            {
                MenuVertical.Visible = false;
                MenuVertical.Width = 285;
                PbLogo.Visible = Visible;
                PanelAnimacion2.ShowSync(MenuVertical);
            }
            else
            {
                MenuVertical.Visible = false;
                MenuVertical.Width = 77;
                PbLogo.Visible = false;
                PanelAnimacion.ShowSync(MenuVertical);
            }
        }

        private void tabClick_Click(object sender, EventArgs e)
        {
            indicador.Top = ((Control)sender).Top;
            indicador.Height = ((Control)sender).Height;
        }

        private void btmLeccion_Click(object sender, EventArgs e)
        {
            
            if (Program.bandiagnos == false)
            {
                AbrirFormInPanel(new Leccion());
            }
            else
            {
                MessageBox.Show("Debes constestar todas las preguntas para salir.");
            }
        }

        private void btmInicio_Click(object sender, EventArgs e)
        {
            if (Program.bandiagnos == false)
            {
                AbrirFormInPanel(new Inicio());
            }
            else
            {
                MessageBox.Show("Debes constestar todas las preguntas para salir.");
            }
        }

        private void PrincipalAlumno_Load(object sender, EventArgs e)
        {
            btmInicio_Click(null, e);
        }

        private void btmDiagnostico_Click(object sender, EventArgs e)
        {
            
            
            if (Program.bandiagnos == false)
            {
                Program.bandiagnos = true;
                AbrirFormInPanel(new diag8());
            }
            else
            {
                MessageBox.Show("Debes constestar todas las preguntas para salir.");
            }

        }

        private void btmCuestionario_Click(object sender, EventArgs e)
        {
            if (Program.bandiagnos == false)
            {
                AbrirFormInPanel(new Examen1());
            }
            else
            {
                MessageBox.Show("Debes constestar todas las preguntas para salir.");
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            IngresarProfesor ip = new IngresarProfesor();
            conexion.CerrarConexion();
            ip.Show();
            this.Close();
        }

        private void panelContenedor_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
