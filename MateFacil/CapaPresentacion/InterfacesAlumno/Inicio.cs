﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
            Bunifu.Framework.Lib.Elipse.Apply(card1, 5);
            Bunifu.Framework.Lib.Elipse.Apply(card2, 5);
            Bunifu.Framework.Lib.Elipse.Apply(card3, 5);
        }
    }
}
