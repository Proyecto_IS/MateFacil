﻿using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Leccion2 : Form
    {
        public void AbrirFormInPanel(object Formhijo)
        {

            if (this.panel1.Controls.Count > 0)
                this.panel1.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(fh);
            this.panel1.Tag = fh;
            fh.Show();

            label1.SendToBack();
            textBox1.SendToBack();
            label2.SendToBack();
            label3.SendToBack();
            pictureBox1.SendToBack();
            button1.SendToBack();
            label4.SendToBack();
            this.AutoScroll = false;
            textBox2.SendToBack();
        }

        public Leccion2()
        {
            InitializeComponent();
        }

        private void Leccion2_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.bandiagnos = true;
            AbrirFormInPanel(new Ejercicios2());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RepasoDiv rd = new RepasoDiv();
            if (rd.ShowDialog() == DialogResult.OK)
            {
                rd.Show();

            }
           
        }
    }
}
