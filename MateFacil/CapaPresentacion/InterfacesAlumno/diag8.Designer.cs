﻿namespace CapaPresentacion.InterfacesAlumno
{
    partial class diag8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(diag8));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton29 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.l = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton44 = new System.Windows.Forms.RadioButton();
            this.label29 = new System.Windows.Forms.Label();
            this.radioButton41 = new System.Windows.Forms.RadioButton();
            this.radioButton43 = new System.Windows.Forms.RadioButton();
            this.radioButton42 = new System.Windows.Forms.RadioButton();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.radioButton45 = new System.Windows.Forms.RadioButton();
            this.radioButton46 = new System.Windows.Forms.RadioButton();
            this.radioButton47 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.radioButton48 = new System.Windows.Forms.RadioButton();
            this.radioButton18 = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.radioButton17 = new System.Windows.Forms.RadioButton();
            this.radioButton16 = new System.Windows.Forms.RadioButton();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioButton32 = new System.Windows.Forms.RadioButton();
            this.label18 = new System.Windows.Forms.Label();
            this.radioButton22 = new System.Windows.Forms.RadioButton();
            this.radioButton21 = new System.Windows.Forms.RadioButton();
            this.radioButton20 = new System.Windows.Forms.RadioButton();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButton30 = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.radioButton26 = new System.Windows.Forms.RadioButton();
            this.radioButton25 = new System.Windows.Forms.RadioButton();
            this.radioButton24 = new System.Windows.Forms.RadioButton();
            this.radioButton23 = new System.Windows.Forms.RadioButton();
            this.button8 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton50 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radioButton49 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton27 = new System.Windows.Forms.RadioButton();
            this.radioButton28 = new System.Windows.Forms.RadioButton();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radioButton51 = new System.Windows.Forms.RadioButton();
            this.radioButton40 = new System.Windows.Forms.RadioButton();
            this.radioButton39 = new System.Windows.Forms.RadioButton();
            this.radioButton38 = new System.Windows.Forms.RadioButton();
            this.radioButton37 = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.radioButton36 = new System.Windows.Forms.RadioButton();
            this.radioButton35 = new System.Windows.Forms.RadioButton();
            this.radioButton34 = new System.Windows.Forms.RadioButton();
            this.radioButton33 = new System.Windows.Forms.RadioButton();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label26 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.textBox4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(85, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(871, 762);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton29);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.pictureBox5);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Location = new System.Drawing.Point(33, 375);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(693, 299);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            // 
            // radioButton29
            // 
            this.radioButton29.AutoSize = true;
            this.radioButton29.Location = new System.Drawing.Point(11, 261);
            this.radioButton29.Name = "radioButton29";
            this.radioButton29.Size = new System.Drawing.Size(161, 17);
            this.radioButton29.TabIndex = 47;
            this.radioButton29.TabStop = true;
            this.radioButton29.Text = "E) Ninguna de las anteriores.";
            this.radioButton29.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(15, 41);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(35, 17);
            this.radioButton1.TabIndex = 39;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "A)";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(61, 205);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(593, 42);
            this.pictureBox5.TabIndex = 45;
            this.pictureBox5.TabStop = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(14, 217);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(36, 17);
            this.radioButton4.TabIndex = 46;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "D)";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(15, 102);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(35, 17);
            this.radioButton2.TabIndex = 40;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "B)";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(15, 160);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(35, 17);
            this.radioButton3.TabIndex = 41;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "C)";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(61, 30);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(560, 42);
            this.pictureBox2.TabIndex = 36;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(61, 150);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(593, 36);
            this.pictureBox4.TabIndex = 38;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(61, 87);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(593, 46);
            this.pictureBox3.TabIndex = 37;
            this.pictureBox3.TabStop = false;
            // 
            // button1
            // 
            this.button1.Image = global::CapaPresentacion.Properties.Resources.adelante;
            this.button1.Location = new System.Drawing.Point(767, 656);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 54);
            this.button1.TabIndex = 44;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(536, 276);
            this.textBox4.MaxLength = 3;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(155, 20);
            this.textBox4.TabIndex = 43;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            this.textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 276);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(505, 17);
            this.label5.TabIndex = 42;
            this.label5.Text = "Dentro de cuantas semanas ambos tendrán la misma cantidad de dinero ahorrada?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 322);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(725, 51);
            this.label4.TabIndex = 35;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(28, 157);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(404, 92);
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(803, 34);
            this.label3.TabIndex = 33;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(716, 51);
            this.label2.TabIndex = 32;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(300, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 25);
            this.label1.TabIndex = 31;
            this.label1.Text = "Ejercicios de Diagnóstico";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.l);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox12);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.button12);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.pictureBox6);
            this.panel2.Controls.Add(this.pictureBox7);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(74, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(877, 754);
            this.panel2.TabIndex = 59;
            // 
            // l
            // 
            this.l.AutoSize = true;
            this.l.Location = new System.Drawing.Point(415, 207);
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(0, 13);
            this.l.TabIndex = 47;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(25, 207);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(440, 17);
            this.label31.TabIndex = 46;
            this.label31.Text = "De las siguientes afirmaciones, marca solamente las que son verdaderas.";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton44);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.radioButton41);
            this.groupBox3.Controls.Add(this.radioButton43);
            this.groupBox3.Controls.Add(this.radioButton42);
            this.groupBox3.Location = new System.Drawing.Point(38, 531);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(286, 110);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            // 
            // radioButton44
            // 
            this.radioButton44.AutoSize = true;
            this.radioButton44.Location = new System.Drawing.Point(6, 11);
            this.radioButton44.Name = "radioButton44";
            this.radioButton44.Size = new System.Drawing.Size(14, 13);
            this.radioButton44.TabIndex = 40;
            this.radioButton44.TabStop = true;
            this.radioButton44.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(26, 11);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(141, 91);
            this.label29.TabIndex = 39;
            this.label29.Text = "A) y = 2.34\r\n\r\nB) y = 2.02\r\n\r\nC) y = 1.49\r\n\r\nD) Ninguna de las anteriores";
            // 
            // radioButton41
            // 
            this.radioButton41.AutoSize = true;
            this.radioButton41.Location = new System.Drawing.Point(6, 40);
            this.radioButton41.Name = "radioButton41";
            this.radioButton41.Size = new System.Drawing.Size(14, 13);
            this.radioButton41.TabIndex = 43;
            this.radioButton41.TabStop = true;
            this.radioButton41.UseVisualStyleBackColor = true;
            // 
            // radioButton43
            // 
            this.radioButton43.AutoSize = true;
            this.radioButton43.Location = new System.Drawing.Point(6, 89);
            this.radioButton43.Name = "radioButton43";
            this.radioButton43.Size = new System.Drawing.Size(14, 13);
            this.radioButton43.TabIndex = 41;
            this.radioButton43.TabStop = true;
            this.radioButton43.UseVisualStyleBackColor = true;
            // 
            // radioButton42
            // 
            this.radioButton42.AutoSize = true;
            this.radioButton42.Location = new System.Drawing.Point(6, 59);
            this.radioButton42.Name = "radioButton42";
            this.radioButton42.Size = new System.Drawing.Size(14, 13);
            this.radioButton42.TabIndex = 42;
            this.radioButton42.TabStop = true;
            this.radioButton42.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.radioButton45);
            this.groupBox12.Controls.Add(this.radioButton46);
            this.groupBox12.Controls.Add(this.radioButton47);
            this.groupBox12.Controls.Add(this.label9);
            this.groupBox12.Location = new System.Drawing.Point(28, 235);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(457, 99);
            this.groupBox12.TabIndex = 44;
            this.groupBox12.TabStop = false;
            // 
            // radioButton45
            // 
            this.radioButton45.AutoSize = true;
            this.radioButton45.Location = new System.Drawing.Point(11, 31);
            this.radioButton45.Name = "radioButton45";
            this.radioButton45.Size = new System.Drawing.Size(14, 13);
            this.radioButton45.TabIndex = 33;
            this.radioButton45.TabStop = true;
            this.radioButton45.UseVisualStyleBackColor = true;
            // 
            // radioButton46
            // 
            this.radioButton46.AutoSize = true;
            this.radioButton46.Location = new System.Drawing.Point(11, 54);
            this.radioButton46.Name = "radioButton46";
            this.radioButton46.Size = new System.Drawing.Size(14, 13);
            this.radioButton46.TabIndex = 34;
            this.radioButton46.TabStop = true;
            this.radioButton46.UseVisualStyleBackColor = true;
            // 
            // radioButton47
            // 
            this.radioButton47.AutoSize = true;
            this.radioButton47.Location = new System.Drawing.Point(11, 79);
            this.radioButton47.Name = "radioButton47";
            this.radioButton47.Size = new System.Drawing.Size(14, 13);
            this.radioButton47.TabIndex = 35;
            this.radioButton47.TabStop = true;
            this.radioButton47.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(333, 65);
            this.label9.TabIndex = 31;
            this.label9.Text = "Cada figura tiene 3 segmentos más que la anterior.\r\n\r\nEl número de segmentos de c" +
    "ada figura es siempre multiplo de 7.\r\n\r\nLas figuras siempre estan formadas por u" +
    "n número par de segmentos.";
            // 
            // button9
            // 
            this.button9.Image = global::CapaPresentacion.Properties.Resources.atras;
            this.button9.Location = new System.Drawing.Point(683, 657);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(55, 52);
            this.button9.TabIndex = 38;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button12
            // 
            this.button12.Image = global::CapaPresentacion.Properties.Resources.adelante;
            this.button12.Location = new System.Drawing.Point(767, 656);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(60, 52);
            this.button12.TabIndex = 37;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(33, 494);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(846, 34);
            this.label30.TabIndex = 36;
            this.label30.Text = "En internet han encontrado la oferta que aparece en la tabla de arriba; cual es l" +
    "a función que mejor aproxima el gasto en relación al número \r\nde mochilas que co" +
    "mpren para los implicados?";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 357);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(703, 34);
            this.label8.TabIndex = 32;
            this.label8.Text = "4. - Los estudiantes que participan en el viaje quieren comprar mochilas para las" +
    " personas implicadas en el proyecto, \r\nque son entre 110 y 130 personas. ";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(36, 65);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(526, 119);
            this.pictureBox6.TabIndex = 30;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(39, 399);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(406, 79);
            this.pictureBox7.TabIndex = 29;
            this.pictureBox7.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(33, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(332, 17);
            this.label10.TabIndex = 28;
            this.label10.Text = "3. - El logo utilizado para el proyecto sigue este patron:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox4);
            this.panel3.Controls.Add(this.button7);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.pictureBox8);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.pictureBox9);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Location = new System.Drawing.Point(61, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(896, 739);
            this.panel3.TabIndex = 60;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.radioButton14);
            this.groupBox4.Controls.Add(this.radioButton13);
            this.groupBox4.Controls.Add(this.radioButton12);
            this.groupBox4.Controls.Add(this.radioButton11);
            this.groupBox4.Location = new System.Drawing.Point(38, 416);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(320, 195);
            this.groupBox4.TabIndex = 57;
            this.groupBox4.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(33, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(252, 143);
            this.label11.TabIndex = 49;
            this.label11.Text = resources.GetString("label11.Text");
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Location = new System.Drawing.Point(3, 18);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(14, 13);
            this.radioButton14.TabIndex = 50;
            this.radioButton14.TabStop = true;
            this.radioButton14.UseVisualStyleBackColor = true;
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Location = new System.Drawing.Point(3, 64);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(14, 13);
            this.radioButton13.TabIndex = 51;
            this.radioButton13.TabStop = true;
            this.radioButton13.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Location = new System.Drawing.Point(3, 99);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(14, 13);
            this.radioButton12.TabIndex = 52;
            this.radioButton12.TabStop = true;
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(3, 137);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(14, 13);
            this.radioButton11.TabIndex = 53;
            this.radioButton11.TabStop = true;
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Image = global::CapaPresentacion.Properties.Resources.atras;
            this.button7.Location = new System.Drawing.Point(683, 657);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(61, 53);
            this.button7.TabIndex = 56;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button3
            // 
            this.button3.Image = global::CapaPresentacion.Properties.Resources.adelante;
            this.button3.Location = new System.Drawing.Point(767, 656);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(61, 53);
            this.button3.TabIndex = 55;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(373, 425);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(335, 186);
            this.pictureBox8.TabIndex = 54;
            this.pictureBox8.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(32, 342);
            this.textBox1.MaxLength = 5;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 45;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(19, 385);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(423, 17);
            this.label12.TabIndex = 48;
            this.label12.Text = "7. - Que significa la publicidad que se muestra en la imagen siguiente?";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 288);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(733, 51);
            this.label13.TabIndex = 47;
            this.label13.Text = resources.GetString("label13.Text");
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(33, 106);
            this.textBox3.MaxLength = 3;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(114, 20);
            this.textBox3.TabIndex = 46;
            this.textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(33, 75);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(355, 17);
            this.label14.TabIndex = 44;
            this.label14.Text = "Qué distancia hay entre el Hotel Descanso y el Hemisféric?";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(390, 75);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(383, 195);
            this.pictureBox9.TabIndex = 43;
            this.pictureBox9.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(19, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(830, 34);
            this.label15.TabIndex = 42;
            this.label15.Text = "5. - En su visita a la playa, los estudiantes se alojaran en el hotel descanso, o" +
    "bserva el siguiente plano y calcula la distancia (en linea recta) \r\nque los sepa" +
    "ra del Hemisféric.";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox7);
            this.panel4.Controls.Add(this.groupBox6);
            this.panel4.Controls.Add(this.groupBox5);
            this.panel4.Controls.Add(this.button8);
            this.panel4.Controls.Add(this.button4);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Location = new System.Drawing.Point(46, 14);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(902, 738);
            this.panel4.TabIndex = 73;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.radioButton48);
            this.groupBox7.Controls.Add(this.radioButton18);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.radioButton17);
            this.groupBox7.Controls.Add(this.radioButton16);
            this.groupBox7.Controls.Add(this.radioButton15);
            this.groupBox7.Location = new System.Drawing.Point(39, 409);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(179, 148);
            this.groupBox7.TabIndex = 58;
            this.groupBox7.TabStop = false;
            // 
            // radioButton48
            // 
            this.radioButton48.AutoSize = true;
            this.radioButton48.Location = new System.Drawing.Point(5, 114);
            this.radioButton48.Name = "radioButton48";
            this.radioButton48.Size = new System.Drawing.Size(173, 17);
            this.radioButton48.TabIndex = 54;
            this.radioButton48.TabStop = true;
            this.radioButton48.Text = "E)     Ninguna de las anteriores.";
            this.radioButton48.UseVisualStyleBackColor = true;
            // 
            // radioButton18
            // 
            this.radioButton18.AutoSize = true;
            this.radioButton18.Location = new System.Drawing.Point(6, 13);
            this.radioButton18.Name = "radioButton18";
            this.radioButton18.Size = new System.Drawing.Size(35, 17);
            this.radioButton18.TabIndex = 50;
            this.radioButton18.TabStop = true;
            this.radioButton18.Text = "A)";
            this.radioButton18.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(47, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 91);
            this.label16.TabIndex = 49;
            this.label16.Text = "45 + 3·x = 11 + x  \r\n\r\n45 + x = 3·(11+x)\r\n\r\n45 – 3·x = 11 + x \r\n\r\n3·(45 + x) = 11" +
    "+x  ";
            // 
            // radioButton17
            // 
            this.radioButton17.AutoSize = true;
            this.radioButton17.Location = new System.Drawing.Point(6, 36);
            this.radioButton17.Name = "radioButton17";
            this.radioButton17.Size = new System.Drawing.Size(35, 17);
            this.radioButton17.TabIndex = 51;
            this.radioButton17.TabStop = true;
            this.radioButton17.Text = "B)";
            this.radioButton17.UseVisualStyleBackColor = true;
            // 
            // radioButton16
            // 
            this.radioButton16.AutoSize = true;
            this.radioButton16.Location = new System.Drawing.Point(6, 68);
            this.radioButton16.Name = "radioButton16";
            this.radioButton16.Size = new System.Drawing.Size(35, 17);
            this.radioButton16.TabIndex = 52;
            this.radioButton16.TabStop = true;
            this.radioButton16.Text = "C)";
            this.radioButton16.UseVisualStyleBackColor = true;
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Location = new System.Drawing.Point(5, 91);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(36, 17);
            this.radioButton15.TabIndex = 53;
            this.radioButton15.TabStop = true;
            this.radioButton15.Text = "D)";
            this.radioButton15.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioButton32);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.radioButton22);
            this.groupBox6.Controls.Add(this.radioButton21);
            this.groupBox6.Controls.Add(this.radioButton20);
            this.groupBox6.Controls.Add(this.radioButton19);
            this.groupBox6.Location = new System.Drawing.Point(22, 226);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(622, 141);
            this.groupBox6.TabIndex = 57;
            this.groupBox6.TabStop = false;
            // 
            // radioButton32
            // 
            this.radioButton32.AutoSize = true;
            this.radioButton32.Location = new System.Drawing.Point(6, 119);
            this.radioButton32.Name = "radioButton32";
            this.radioButton32.Size = new System.Drawing.Size(173, 17);
            this.radioButton32.TabIndex = 49;
            this.radioButton32.TabStop = true;
            this.radioButton32.Text = "E)     Ninguna de las anteriores.";
            this.radioButton32.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(49, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(571, 91);
            this.label18.TabIndex = 43;
            this.label18.Text = resources.GetString("label18.Text");
            // 
            // radioButton22
            // 
            this.radioButton22.AutoSize = true;
            this.radioButton22.Location = new System.Drawing.Point(7, 22);
            this.radioButton22.Name = "radioButton22";
            this.radioButton22.Size = new System.Drawing.Size(35, 17);
            this.radioButton22.TabIndex = 44;
            this.radioButton22.TabStop = true;
            this.radioButton22.Text = "A)";
            this.radioButton22.UseVisualStyleBackColor = true;
            // 
            // radioButton21
            // 
            this.radioButton21.AutoSize = true;
            this.radioButton21.Location = new System.Drawing.Point(7, 52);
            this.radioButton21.Name = "radioButton21";
            this.radioButton21.Size = new System.Drawing.Size(35, 17);
            this.radioButton21.TabIndex = 45;
            this.radioButton21.TabStop = true;
            this.radioButton21.Text = "B)";
            this.radioButton21.UseVisualStyleBackColor = true;
            // 
            // radioButton20
            // 
            this.radioButton20.AutoSize = true;
            this.radioButton20.Location = new System.Drawing.Point(7, 75);
            this.radioButton20.Name = "radioButton20";
            this.radioButton20.Size = new System.Drawing.Size(35, 17);
            this.radioButton20.TabIndex = 46;
            this.radioButton20.TabStop = true;
            this.radioButton20.Text = "C)";
            this.radioButton20.UseVisualStyleBackColor = true;
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.Location = new System.Drawing.Point(7, 98);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(36, 17);
            this.radioButton19.TabIndex = 47;
            this.radioButton19.TabStop = true;
            this.radioButton19.Text = "D)";
            this.radioButton19.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButton30);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.radioButton26);
            this.groupBox5.Controls.Add(this.radioButton25);
            this.groupBox5.Controls.Add(this.radioButton24);
            this.groupBox5.Controls.Add(this.radioButton23);
            this.groupBox5.Location = new System.Drawing.Point(28, 65);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(339, 137);
            this.groupBox5.TabIndex = 56;
            this.groupBox5.TabStop = false;
            // 
            // radioButton30
            // 
            this.radioButton30.AutoSize = true;
            this.radioButton30.Location = new System.Drawing.Point(3, 114);
            this.radioButton30.Name = "radioButton30";
            this.radioButton30.Size = new System.Drawing.Size(200, 17);
            this.radioButton30.TabIndex = 48;
            this.radioButton30.TabStop = true;
            this.radioButton30.Text = "    E)          Ninguna de las anteriores.";
            this.radioButton30.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(31, 15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(308, 91);
            this.label20.TabIndex = 37;
            this.label20.Text = resources.GetString("label20.Text");
            // 
            // radioButton26
            // 
            this.radioButton26.AutoSize = true;
            this.radioButton26.Location = new System.Drawing.Point(3, 15);
            this.radioButton26.Name = "radioButton26";
            this.radioButton26.Size = new System.Drawing.Size(14, 13);
            this.radioButton26.TabIndex = 38;
            this.radioButton26.TabStop = true;
            this.radioButton26.UseVisualStyleBackColor = true;
            // 
            // radioButton25
            // 
            this.radioButton25.AutoSize = true;
            this.radioButton25.Location = new System.Drawing.Point(3, 46);
            this.radioButton25.Name = "radioButton25";
            this.radioButton25.Size = new System.Drawing.Size(14, 13);
            this.radioButton25.TabIndex = 39;
            this.radioButton25.TabStop = true;
            this.radioButton25.UseVisualStyleBackColor = true;
            // 
            // radioButton24
            // 
            this.radioButton24.AutoSize = true;
            this.radioButton24.Location = new System.Drawing.Point(3, 65);
            this.radioButton24.Name = "radioButton24";
            this.radioButton24.Size = new System.Drawing.Size(14, 13);
            this.radioButton24.TabIndex = 40;
            this.radioButton24.TabStop = true;
            this.radioButton24.UseVisualStyleBackColor = true;
            // 
            // radioButton23
            // 
            this.radioButton23.AutoSize = true;
            this.radioButton23.Location = new System.Drawing.Point(3, 93);
            this.radioButton23.Name = "radioButton23";
            this.radioButton23.Size = new System.Drawing.Size(14, 13);
            this.radioButton23.TabIndex = 41;
            this.radioButton23.TabStop = true;
            this.radioButton23.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Image = global::CapaPresentacion.Properties.Resources.atras;
            this.button8.Location = new System.Drawing.Point(683, 657);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(57, 56);
            this.button8.TabIndex = 55;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button4
            // 
            this.button4.Image = global::CapaPresentacion.Properties.Resources.adelante;
            this.button4.Location = new System.Drawing.Point(767, 656);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(63, 56);
            this.button4.TabIndex = 54;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(19, 369);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(842, 34);
            this.label17.TabIndex = 48;
            this.label17.Text = resources.GetString("label17.Text");
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(19, 205);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(407, 17);
            this.label19.TabIndex = 42;
            this.label19.Text = "9. - Una de las siguientes situaciones NO depende del azar. Cual es?";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 13);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(739, 51);
            this.label21.TabIndex = 36;
            this.label21.Text = resources.GetString("label21.Text");
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox2);
            this.panel5.Controls.Add(this.groupBox8);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.pictureBox10);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.button5);
            this.panel5.Location = new System.Drawing.Point(31, 14);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(927, 729);
            this.panel5.TabIndex = 75;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton50);
            this.groupBox2.Controls.Add(this.radioButton5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.radioButton6);
            this.groupBox2.Controls.Add(this.radioButton7);
            this.groupBox2.Controls.Add(this.radioButton8);
            this.groupBox2.Location = new System.Drawing.Point(32, 508);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 179);
            this.groupBox2.TabIndex = 71;
            this.groupBox2.TabStop = false;
            // 
            // radioButton50
            // 
            this.radioButton50.AutoSize = true;
            this.radioButton50.Location = new System.Drawing.Point(9, 130);
            this.radioButton50.Name = "radioButton50";
            this.radioButton50.Size = new System.Drawing.Size(173, 17);
            this.radioButton50.TabIndex = 70;
            this.radioButton50.TabStop = true;
            this.radioButton50.Text = "E)     Ninguna de las anteriores.";
            this.radioButton50.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(8, 26);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(35, 17);
            this.radioButton5.TabIndex = 66;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "A)";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(60, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 91);
            this.label6.TabIndex = 65;
            this.label6.Text = "$ .964 por litro\r\n\r\n$ .986 por litro\r\n\r\n$ 1.143 por litro\r\n\r\n$ 1.15 por litro";
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(9, 105);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(36, 17);
            this.radioButton6.TabIndex = 69;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "D)";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(8, 52);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(35, 17);
            this.radioButton7.TabIndex = 67;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "B)";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(8, 78);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(35, 17);
            this.radioButton8.TabIndex = 68;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "C)";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radioButton49);
            this.groupBox8.Controls.Add(this.radioButton9);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.radioButton10);
            this.groupBox8.Controls.Add(this.radioButton27);
            this.groupBox8.Controls.Add(this.radioButton28);
            this.groupBox8.Location = new System.Drawing.Point(44, 54);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(188, 138);
            this.groupBox8.TabIndex = 70;
            this.groupBox8.TabStop = false;
            // 
            // radioButton49
            // 
            this.radioButton49.AutoSize = true;
            this.radioButton49.Location = new System.Drawing.Point(6, 115);
            this.radioButton49.Name = "radioButton49";
            this.radioButton49.Size = new System.Drawing.Size(173, 17);
            this.radioButton49.TabIndex = 62;
            this.radioButton49.TabStop = true;
            this.radioButton49.Text = "E)     Ninguna de las anteriores.";
            this.radioButton49.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(6, 19);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(35, 17);
            this.radioButton9.TabIndex = 58;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "A)";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 91);
            this.label7.TabIndex = 57;
            this.label7.Text = "$1,000.00\r\n\r\n$4,000.00\r\n\r\n$8,000.00\r\n\r\n$15,000.00";
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(6, 43);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(35, 17);
            this.radioButton10.TabIndex = 59;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "B)";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton27
            // 
            this.radioButton27.AutoSize = true;
            this.radioButton27.Location = new System.Drawing.Point(6, 71);
            this.radioButton27.Name = "radioButton27";
            this.radioButton27.Size = new System.Drawing.Size(35, 17);
            this.radioButton27.TabIndex = 60;
            this.radioButton27.TabStop = true;
            this.radioButton27.Text = "C)";
            this.radioButton27.UseVisualStyleBackColor = true;
            // 
            // radioButton28
            // 
            this.radioButton28.AutoSize = true;
            this.radioButton28.Location = new System.Drawing.Point(5, 95);
            this.radioButton28.Name = "radioButton28";
            this.radioButton28.Size = new System.Drawing.Size(36, 17);
            this.radioButton28.TabIndex = 61;
            this.radioButton28.TabStop = true;
            this.radioButton28.Text = "D)";
            this.radioButton28.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(28, 487);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(333, 17);
            this.label22.TabIndex = 64;
            this.label22.Text = "Cual ha sido el precio promedio durante esta semana?";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(53, 222);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(352, 233);
            this.pictureBox10.TabIndex = 63;
            this.pictureBox10.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(20, 195);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(526, 17);
            this.label23.TabIndex = 62;
            this.label23.Text = "12. -  El precio de la gasolina ha variado mucho durante los dias que ha durado e" +
    "l viaje. ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(38, 14);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(922, 34);
            this.label24.TabIndex = 56;
            this.label24.Text = resources.GetString("label24.Text");
            // 
            // button2
            // 
            this.button2.Image = global::CapaPresentacion.Properties.Resources.atras;
            this.button2.Location = new System.Drawing.Point(683, 657);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 56);
            this.button2.TabIndex = 55;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button5
            // 
            this.button5.Image = global::CapaPresentacion.Properties.Resources.adelante;
            this.button5.Location = new System.Drawing.Point(767, 656);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(63, 56);
            this.button5.TabIndex = 54;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.groupBox11);
            this.panel6.Controls.Add(this.groupBox10);
            this.panel6.Controls.Add(this.textBox5);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.button11);
            this.panel6.Controls.Add(this.pictureBox12);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.pictureBox11);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.button10);
            this.panel6.Location = new System.Drawing.Point(11, 14);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(946, 719);
            this.panel6.TabIndex = 76;
            this.panel6.Visible = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label32);
            this.panel7.Location = new System.Drawing.Point(748, -9);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(227, 293);
            this.panel7.TabIndex = 77;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(285, 51);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(16, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "rrr";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radioButton51);
            this.groupBox11.Controls.Add(this.radioButton40);
            this.groupBox11.Controls.Add(this.radioButton39);
            this.groupBox11.Controls.Add(this.radioButton38);
            this.groupBox11.Controls.Add(this.radioButton37);
            this.groupBox11.Location = new System.Drawing.Point(44, 271);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(171, 178);
            this.groupBox11.TabIndex = 76;
            this.groupBox11.TabStop = false;
            // 
            // radioButton51
            // 
            this.radioButton51.AutoSize = true;
            this.radioButton51.Location = new System.Drawing.Point(1, 130);
            this.radioButton51.Name = "radioButton51";
            this.radioButton51.Size = new System.Drawing.Size(173, 17);
            this.radioButton51.TabIndex = 70;
            this.radioButton51.TabStop = true;
            this.radioButton51.Text = "E)     Ninguna de las anteriores.";
            this.radioButton51.UseVisualStyleBackColor = true;
            // 
            // radioButton40
            // 
            this.radioButton40.AutoSize = true;
            this.radioButton40.Location = new System.Drawing.Point(6, 19);
            this.radioButton40.Name = "radioButton40";
            this.radioButton40.Size = new System.Drawing.Size(81, 17);
            this.radioButton40.TabIndex = 66;
            this.radioButton40.TabStop = true;
            this.radioButton40.Text = "A) Gráfica 1";
            this.radioButton40.UseVisualStyleBackColor = true;
            // 
            // radioButton39
            // 
            this.radioButton39.AutoSize = true;
            this.radioButton39.Location = new System.Drawing.Point(6, 46);
            this.radioButton39.Name = "radioButton39";
            this.radioButton39.Size = new System.Drawing.Size(81, 17);
            this.radioButton39.TabIndex = 67;
            this.radioButton39.TabStop = true;
            this.radioButton39.Text = "B) Gráfica 2";
            this.radioButton39.UseVisualStyleBackColor = true;
            // 
            // radioButton38
            // 
            this.radioButton38.AutoSize = true;
            this.radioButton38.Location = new System.Drawing.Point(6, 74);
            this.radioButton38.Name = "radioButton38";
            this.radioButton38.Size = new System.Drawing.Size(81, 17);
            this.radioButton38.TabIndex = 68;
            this.radioButton38.TabStop = true;
            this.radioButton38.Text = "C) Gráfica 3";
            this.radioButton38.UseVisualStyleBackColor = true;
            // 
            // radioButton37
            // 
            this.radioButton37.AutoSize = true;
            this.radioButton37.Location = new System.Drawing.Point(6, 102);
            this.radioButton37.Name = "radioButton37";
            this.radioButton37.Size = new System.Drawing.Size(82, 17);
            this.radioButton37.TabIndex = 69;
            this.radioButton37.TabStop = true;
            this.radioButton37.Text = "D) Gráfica 4";
            this.radioButton37.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.radioButton36);
            this.groupBox10.Controls.Add(this.radioButton35);
            this.groupBox10.Controls.Add(this.radioButton34);
            this.groupBox10.Controls.Add(this.radioButton33);
            this.groupBox10.Location = new System.Drawing.Point(29, 42);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(166, 137);
            this.groupBox10.TabIndex = 75;
            this.groupBox10.TabStop = false;
            // 
            // radioButton36
            // 
            this.radioButton36.AutoSize = true;
            this.radioButton36.Location = new System.Drawing.Point(6, 20);
            this.radioButton36.Name = "radioButton36";
            this.radioButton36.Size = new System.Drawing.Size(50, 17);
            this.radioButton36.TabIndex = 58;
            this.radioButton36.TabStop = true;
            this.radioButton36.Text = "A) 24";
            this.radioButton36.UseVisualStyleBackColor = true;
            // 
            // radioButton35
            // 
            this.radioButton35.AutoSize = true;
            this.radioButton35.Location = new System.Drawing.Point(6, 50);
            this.radioButton35.Name = "radioButton35";
            this.radioButton35.Size = new System.Drawing.Size(50, 17);
            this.radioButton35.TabIndex = 59;
            this.radioButton35.TabStop = true;
            this.radioButton35.Text = "B) 26";
            this.radioButton35.UseVisualStyleBackColor = true;
            // 
            // radioButton34
            // 
            this.radioButton34.AutoSize = true;
            this.radioButton34.Location = new System.Drawing.Point(6, 79);
            this.radioButton34.Name = "radioButton34";
            this.radioButton34.Size = new System.Drawing.Size(50, 17);
            this.radioButton34.TabIndex = 60;
            this.radioButton34.TabStop = true;
            this.radioButton34.Text = "C) 52";
            this.radioButton34.UseVisualStyleBackColor = true;
            // 
            // radioButton33
            // 
            this.radioButton33.AutoSize = true;
            this.radioButton33.Location = new System.Drawing.Point(6, 107);
            this.radioButton33.Name = "radioButton33";
            this.radioButton33.Size = new System.Drawing.Size(159, 17);
            this.radioButton33.TabIndex = 61;
            this.radioButton33.TabStop = true;
            this.radioButton33.Text = "D) Ninguna de las anteriores\r\n";
            this.radioButton33.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(45, 531);
            this.textBox5.MaxLength = 4;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 74;
            this.textBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox5_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(17, 502);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(826, 17);
            this.label28.TabIndex = 73;
            this.label28.Text = "15. - Sergio tiene cuatro cajas llenas de jarras. Cada caja tiene cuatro filas y " +
    "cada fila contiene cuatro jarras. ¿Cuántas jarras hay en total?";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(767, 656);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(64, 56);
            this.button11.TabIndex = 72;
            this.button11.Text = "Finalizar";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(211, 269);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(350, 209);
            this.pictureBox12.TabIndex = 65;
            this.pictureBox12.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(17, 229);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(695, 34);
            this.label25.TabIndex = 62;
            this.label25.Text = resources.GetString("label25.Text");
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(234, 43);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(248, 165);
            this.pictureBox11.TabIndex = 57;
            this.pictureBox11.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(17, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(268, 17);
            this.label26.TabIndex = 56;
            this.label26.Text = "13. - ¿Qué perímetro tiene el triángulo ABC?";
            // 
            // button10
            // 
            this.button10.Image = global::CapaPresentacion.Properties.Resources.atras;
            this.button10.Location = new System.Drawing.Point(683, 657);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(57, 56);
            this.button10.TabIndex = 55;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.AutoSize = true;
            this.btnCerrar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCerrar.Location = new System.Drawing.Point(964, 9);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(17, 17);
            this.btnCerrar.TabIndex = 77;
            this.btnCerrar.Text = "X";
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // diag8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(987, 788);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "diag8";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "diag8";
            this.Load += new System.EventHandler(this.diag8_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label l;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton44;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.RadioButton radioButton41;
        private System.Windows.Forms.RadioButton radioButton43;
        private System.Windows.Forms.RadioButton radioButton42;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton radioButton45;
        private System.Windows.Forms.RadioButton radioButton46;
        private System.Windows.Forms.RadioButton radioButton47;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radioButton18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton radioButton17;
        private System.Windows.Forms.RadioButton radioButton16;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RadioButton radioButton22;
        private System.Windows.Forms.RadioButton radioButton21;
        private System.Windows.Forms.RadioButton radioButton20;
        private System.Windows.Forms.RadioButton radioButton19;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.RadioButton radioButton26;
        private System.Windows.Forms.RadioButton radioButton25;
        private System.Windows.Forms.RadioButton radioButton24;
        private System.Windows.Forms.RadioButton radioButton23;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton27;
        private System.Windows.Forms.RadioButton radioButton28;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radioButton40;
        private System.Windows.Forms.RadioButton radioButton39;
        private System.Windows.Forms.RadioButton radioButton38;
        private System.Windows.Forms.RadioButton radioButton37;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton radioButton36;
        private System.Windows.Forms.RadioButton radioButton35;
        private System.Windows.Forms.RadioButton radioButton34;
        private System.Windows.Forms.RadioButton radioButton33;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label btnCerrar;
        private System.Windows.Forms.RadioButton radioButton29;
        private System.Windows.Forms.RadioButton radioButton48;
        private System.Windows.Forms.RadioButton radioButton32;
        private System.Windows.Forms.RadioButton radioButton30;
        private System.Windows.Forms.RadioButton radioButton50;
        private System.Windows.Forms.RadioButton radioButton49;
        private System.Windows.Forms.RadioButton radioButton51;
    }
}