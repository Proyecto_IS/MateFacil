﻿using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Leccion3 : Form
    {
        public Leccion3()
        {
            InitializeComponent();
        }

        public void AbrirFormInPanel(object Formhijo)
        {
            panel1.BringToFront();
            if (this.panel1.Controls.Count > 0)
                this.panel1.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(fh);
            this.panel1.Tag = fh;
            fh.Show();

            
            this.AutoScroll = false;
           
        }

        private void Leccion3_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.bandiagnos = true;
            AbrirFormInPanel(new Ejercicios3());
        }
    }
}
