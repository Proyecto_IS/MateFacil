﻿namespace CapaPresentacion.InterfacesAlumno
{
    partial class PrincipalAlumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrincipalAlumno));
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Menu = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.iconminimizar = new System.Windows.Forms.PictureBox();
            this.iconrestaurar = new System.Windows.Forms.PictureBox();
            this.iconcerrar = new System.Windows.Forms.PictureBox();
            this.iconmaximizar = new System.Windows.Forms.PictureBox();
            this.MenuVertical = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.indicador = new System.Windows.Forms.PictureBox();
            this.btmLeccion = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmDiagnostico = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmCuestionario = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmInicio = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.PbLogo = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panelContenedor = new System.Windows.Forms.Panel();
            this.PanelAnimacion2 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.PanelAnimacion = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconminimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconrestaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconcerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconmaximizar)).BeginInit();
            this.MenuVertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.indicador)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(12)))), ((int)(((byte)(14)))));
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Menu);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.iconminimizar);
            this.panel1.Controls.Add(this.iconrestaurar);
            this.panel1.Controls.Add(this.iconcerrar);
            this.panel1.Controls.Add(this.iconmaximizar);
            this.PanelAnimacion.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1250, 48);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // pictureBox5
            // 
            this.PanelAnimacion2.SetDecoration(this.pictureBox5, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.pictureBox5, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(954, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(37, 29);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion2.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(987, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 21);
            this.label3.TabIndex = 11;
            this.label3.Text = "Cerrar Sesión";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Menu
            // 
            this.Menu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion2.SetDecoration(this.Menu, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.Menu, BunifuAnimatorNS.DecorationType.None);
            this.Menu.Enabled = false;
            this.Menu.Image = ((System.Drawing.Image)(resources.GetObject("Menu.Image")));
            this.Menu.Location = new System.Drawing.Point(271, 10);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(33, 32);
            this.Menu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Menu.TabIndex = 10;
            this.Menu.TabStop = false;
            this.Menu.Visible = false;
            this.Menu.Click += new System.EventHandler(this.Menu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.PanelAnimacion2.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(56, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Bienvenido";
            // 
            // iconminimizar
            // 
            this.iconminimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconminimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion2.SetDecoration(this.iconminimizar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconminimizar, BunifuAnimatorNS.DecorationType.None);
            this.iconminimizar.Image = ((System.Drawing.Image)(resources.GetObject("iconminimizar.Image")));
            this.iconminimizar.Location = new System.Drawing.Point(1182, 3);
            this.iconminimizar.Name = "iconminimizar";
            this.iconminimizar.Size = new System.Drawing.Size(25, 25);
            this.iconminimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconminimizar.TabIndex = 8;
            this.iconminimizar.TabStop = false;
            this.iconminimizar.Click += new System.EventHandler(this.iconminimizar_Click_1);
            // 
            // iconrestaurar
            // 
            this.iconrestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconrestaurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion2.SetDecoration(this.iconrestaurar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconrestaurar, BunifuAnimatorNS.DecorationType.None);
            this.iconrestaurar.Enabled = false;
            this.iconrestaurar.Image = ((System.Drawing.Image)(resources.GetObject("iconrestaurar.Image")));
            this.iconrestaurar.Location = new System.Drawing.Point(733, 6);
            this.iconrestaurar.Name = "iconrestaurar";
            this.iconrestaurar.Size = new System.Drawing.Size(25, 25);
            this.iconrestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconrestaurar.TabIndex = 7;
            this.iconrestaurar.TabStop = false;
            this.iconrestaurar.Visible = false;
            this.iconrestaurar.Click += new System.EventHandler(this.iconrestaurar_Click_1);
            // 
            // iconcerrar
            // 
            this.iconcerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconcerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion2.SetDecoration(this.iconcerrar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconcerrar, BunifuAnimatorNS.DecorationType.None);
            this.iconcerrar.Image = ((System.Drawing.Image)(resources.GetObject("iconcerrar.Image")));
            this.iconcerrar.Location = new System.Drawing.Point(1213, 3);
            this.iconcerrar.Name = "iconcerrar";
            this.iconcerrar.Size = new System.Drawing.Size(25, 25);
            this.iconcerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconcerrar.TabIndex = 5;
            this.iconcerrar.TabStop = false;
            this.iconcerrar.Click += new System.EventHandler(this.iconcerrar_Click_1);
            // 
            // iconmaximizar
            // 
            this.iconmaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconmaximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion2.SetDecoration(this.iconmaximizar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconmaximizar, BunifuAnimatorNS.DecorationType.None);
            this.iconmaximizar.Enabled = false;
            this.iconmaximizar.Image = ((System.Drawing.Image)(resources.GetObject("iconmaximizar.Image")));
            this.iconmaximizar.Location = new System.Drawing.Point(733, 6);
            this.iconmaximizar.Name = "iconmaximizar";
            this.iconmaximizar.Size = new System.Drawing.Size(25, 25);
            this.iconmaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconmaximizar.TabIndex = 6;
            this.iconmaximizar.TabStop = false;
            this.iconmaximizar.Visible = false;
            this.iconmaximizar.Click += new System.EventHandler(this.iconmaximizar_Click_1);
            // 
            // MenuVertical
            // 
            this.MenuVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.MenuVertical.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MenuVertical.Controls.Add(this.pictureBox3);
            this.MenuVertical.Controls.Add(this.pictureBox2);
            this.MenuVertical.Controls.Add(this.pictureBox1);
            this.MenuVertical.Controls.Add(this.indicador);
            this.MenuVertical.Controls.Add(this.btmLeccion);
            this.MenuVertical.Controls.Add(this.btmDiagnostico);
            this.MenuVertical.Controls.Add(this.btmCuestionario);
            this.MenuVertical.Controls.Add(this.btmInicio);
            this.MenuVertical.Controls.Add(this.panel4);
            this.MenuVertical.Controls.Add(this.pictureBox4);
            this.PanelAnimacion.SetDecoration(this.MenuVertical, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.MenuVertical, BunifuAnimatorNS.DecorationType.None);
            this.MenuVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuVertical.Location = new System.Drawing.Point(0, 48);
            this.MenuVertical.Name = "MenuVertical";
            this.MenuVertical.Size = new System.Drawing.Size(285, 602);
            this.MenuVertical.TabIndex = 1;
            this.MenuVertical.Paint += new System.Windows.Forms.PaintEventHandler(this.MenuVertical_Paint);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.PanelAnimacion2.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox3.Location = new System.Drawing.Point(3, 503);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(6, 50);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.PanelAnimacion2.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox2.Location = new System.Drawing.Point(3, 421);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(6, 50);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.PanelAnimacion2.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox1.Location = new System.Drawing.Point(3, 332);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(6, 50);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // indicador
            // 
            this.indicador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.PanelAnimacion2.SetDecoration(this.indicador, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.indicador, BunifuAnimatorNS.DecorationType.None);
            this.indicador.Location = new System.Drawing.Point(3, 245);
            this.indicador.Name = "indicador";
            this.indicador.Size = new System.Drawing.Size(6, 50);
            this.indicador.TabIndex = 3;
            this.indicador.TabStop = false;
            // 
            // btmLeccion
            // 
            this.btmLeccion.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmLeccion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmLeccion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmLeccion.BorderRadius = 0;
            this.btmLeccion.ButtonText = "      Tomar Lección";
            this.btmLeccion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion.SetDecoration(this.btmLeccion, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmLeccion, BunifuAnimatorNS.DecorationType.None);
            this.btmLeccion.DisabledColor = System.Drawing.Color.Gray;
            this.btmLeccion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmLeccion.Iconcolor = System.Drawing.Color.Transparent;
            this.btmLeccion.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmLeccion.Iconimage")));
            this.btmLeccion.Iconimage_right = null;
            this.btmLeccion.Iconimage_right_Selected = null;
            this.btmLeccion.Iconimage_Selected = null;
            this.btmLeccion.IconMarginLeft = 20;
            this.btmLeccion.IconMarginRight = 0;
            this.btmLeccion.IconRightVisible = true;
            this.btmLeccion.IconRightZoom = 0D;
            this.btmLeccion.IconVisible = true;
            this.btmLeccion.IconZoom = 80D;
            this.btmLeccion.IsTab = true;
            this.btmLeccion.Location = new System.Drawing.Point(4, 421);
            this.btmLeccion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmLeccion.Name = "btmLeccion";
            this.btmLeccion.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmLeccion.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmLeccion.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmLeccion.selected = false;
            this.btmLeccion.Size = new System.Drawing.Size(278, 50);
            this.btmLeccion.TabIndex = 2;
            this.btmLeccion.Text = "      Tomar Lección";
            this.btmLeccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmLeccion.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmLeccion.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmLeccion.Click += new System.EventHandler(this.btmLeccion_Click);
            // 
            // btmDiagnostico
            // 
            this.btmDiagnostico.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmDiagnostico.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmDiagnostico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmDiagnostico.BorderRadius = 0;
            this.btmDiagnostico.ButtonText = "      Tomar Diagnóstico";
            this.btmDiagnostico.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion.SetDecoration(this.btmDiagnostico, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmDiagnostico, BunifuAnimatorNS.DecorationType.None);
            this.btmDiagnostico.DisabledColor = System.Drawing.Color.Gray;
            this.btmDiagnostico.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmDiagnostico.Iconcolor = System.Drawing.Color.Transparent;
            this.btmDiagnostico.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmDiagnostico.Iconimage")));
            this.btmDiagnostico.Iconimage_right = null;
            this.btmDiagnostico.Iconimage_right_Selected = null;
            this.btmDiagnostico.Iconimage_Selected = null;
            this.btmDiagnostico.IconMarginLeft = 20;
            this.btmDiagnostico.IconMarginRight = 0;
            this.btmDiagnostico.IconRightVisible = true;
            this.btmDiagnostico.IconRightZoom = 0D;
            this.btmDiagnostico.IconVisible = true;
            this.btmDiagnostico.IconZoom = 80D;
            this.btmDiagnostico.IsTab = true;
            this.btmDiagnostico.Location = new System.Drawing.Point(0, 332);
            this.btmDiagnostico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmDiagnostico.Name = "btmDiagnostico";
            this.btmDiagnostico.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmDiagnostico.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmDiagnostico.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmDiagnostico.selected = false;
            this.btmDiagnostico.Size = new System.Drawing.Size(285, 50);
            this.btmDiagnostico.TabIndex = 2;
            this.btmDiagnostico.Text = "      Tomar Diagnóstico";
            this.btmDiagnostico.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmDiagnostico.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmDiagnostico.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmDiagnostico.Click += new System.EventHandler(this.btmDiagnostico_Click);
            // 
            // btmCuestionario
            // 
            this.btmCuestionario.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmCuestionario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmCuestionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmCuestionario.BorderRadius = 0;
            this.btmCuestionario.ButtonText = "      Tomar Cuestionario";
            this.btmCuestionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion.SetDecoration(this.btmCuestionario, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmCuestionario, BunifuAnimatorNS.DecorationType.None);
            this.btmCuestionario.DisabledColor = System.Drawing.Color.Gray;
            this.btmCuestionario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmCuestionario.Iconcolor = System.Drawing.Color.Transparent;
            this.btmCuestionario.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmCuestionario.Iconimage")));
            this.btmCuestionario.Iconimage_right = null;
            this.btmCuestionario.Iconimage_right_Selected = null;
            this.btmCuestionario.Iconimage_Selected = null;
            this.btmCuestionario.IconMarginLeft = 20;
            this.btmCuestionario.IconMarginRight = 0;
            this.btmCuestionario.IconRightVisible = true;
            this.btmCuestionario.IconRightZoom = 0D;
            this.btmCuestionario.IconVisible = true;
            this.btmCuestionario.IconZoom = 80D;
            this.btmCuestionario.IsTab = true;
            this.btmCuestionario.Location = new System.Drawing.Point(4, 503);
            this.btmCuestionario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmCuestionario.Name = "btmCuestionario";
            this.btmCuestionario.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmCuestionario.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmCuestionario.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmCuestionario.selected = false;
            this.btmCuestionario.Size = new System.Drawing.Size(278, 50);
            this.btmCuestionario.TabIndex = 2;
            this.btmCuestionario.Text = "      Tomar Cuestionario";
            this.btmCuestionario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmCuestionario.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmCuestionario.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmCuestionario.Click += new System.EventHandler(this.btmCuestionario_Click);
            // 
            // btmInicio
            // 
            this.btmInicio.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmInicio.BackColor = System.Drawing.Color.Gainsboro;
            this.btmInicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmInicio.BorderRadius = 0;
            this.btmInicio.ButtonText = "      Inicio";
            this.btmInicio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelAnimacion.SetDecoration(this.btmInicio, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmInicio, BunifuAnimatorNS.DecorationType.None);
            this.btmInicio.DisabledColor = System.Drawing.Color.Gray;
            this.btmInicio.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmInicio.Iconcolor = System.Drawing.Color.Transparent;
            this.btmInicio.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmInicio.Iconimage")));
            this.btmInicio.Iconimage_right = null;
            this.btmInicio.Iconimage_right_Selected = null;
            this.btmInicio.Iconimage_Selected = null;
            this.btmInicio.IconMarginLeft = 20;
            this.btmInicio.IconMarginRight = 0;
            this.btmInicio.IconRightVisible = true;
            this.btmInicio.IconRightZoom = 0D;
            this.btmInicio.IconVisible = true;
            this.btmInicio.IconZoom = 80D;
            this.btmInicio.IsTab = true;
            this.btmInicio.Location = new System.Drawing.Point(0, 245);
            this.btmInicio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmInicio.Name = "btmInicio";
            this.btmInicio.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmInicio.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmInicio.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmInicio.selected = true;
            this.btmInicio.Size = new System.Drawing.Size(285, 50);
            this.btmInicio.TabIndex = 2;
            this.btmInicio.Text = "      Inicio";
            this.btmInicio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmInicio.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmInicio.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmInicio.Click += new System.EventHandler(this.btmInicio_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.panel4.Controls.Add(this.PbLogo);
            this.panel4.Controls.Add(this.label2);
            this.PanelAnimacion.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(283, 197);
            this.panel4.TabIndex = 2;
            // 
            // PbLogo
            // 
            this.PanelAnimacion2.SetDecoration(this.PbLogo, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.PbLogo, BunifuAnimatorNS.DecorationType.None);
            this.PbLogo.Image = ((System.Drawing.Image)(resources.GetObject("PbLogo.Image")));
            this.PbLogo.Location = new System.Drawing.Point(59, 2);
            this.PbLogo.Name = "PbLogo";
            this.PbLogo.Size = new System.Drawing.Size(187, 158);
            this.PbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PbLogo.TabIndex = 0;
            this.PbLogo.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.PanelAnimacion2.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(86, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 22);
            this.label2.TabIndex = 9;
            this.label2.Text = "MateFacil-PC";
            // 
            // pictureBox4
            // 
            this.PanelAnimacion2.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(283, 600);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // panelContenedor
            // 
            this.panelContenedor.BackColor = System.Drawing.Color.Transparent;
            this.panelContenedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelContenedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAnimacion.SetDecoration(this.panelContenedor, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.panelContenedor, BunifuAnimatorNS.DecorationType.None);
            this.panelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedor.Location = new System.Drawing.Point(285, 48);
            this.panelContenedor.Name = "panelContenedor";
            this.panelContenedor.Size = new System.Drawing.Size(965, 602);
            this.panelContenedor.TabIndex = 2;
            this.panelContenedor.Paint += new System.Windows.Forms.PaintEventHandler(this.panelContenedor_Paint);
            // 
            // PanelAnimacion2
            // 
            this.PanelAnimacion2.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.PanelAnimacion2.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.PanelAnimacion2.DefaultAnimation = animation1;
            // 
            // PanelAnimacion
            // 
            this.PanelAnimacion.AnimationType = BunifuAnimatorNS.AnimationType.Mosaic;
            this.PanelAnimacion.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 20;
            animation2.Padding = new System.Windows.Forms.Padding(30);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.PanelAnimacion.DefaultAnimation = animation2;
            // 
            // PrincipalAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1250, 650);
            this.Controls.Add(this.panelContenedor);
            this.Controls.Add(this.MenuVertical);
            this.Controls.Add(this.panel1);
            this.PanelAnimacion2.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PrincipalAlumno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrincipalAlumno";
            this.Load += new System.EventHandler(this.PrincipalAlumno_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconminimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconrestaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconcerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconmaximizar)).EndInit();
            this.MenuVertical.ResumeLayout(false);
            this.MenuVertical.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.indicador)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox iconminimizar;
        private System.Windows.Forms.PictureBox iconmaximizar;
        private System.Windows.Forms.PictureBox iconrestaurar;
        private System.Windows.Forms.PictureBox iconcerrar;
        private System.Windows.Forms.Panel MenuVertical;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuFlatButton btmLeccion;
        private Bunifu.Framework.UI.BunifuFlatButton btmCuestionario;
        private Bunifu.Framework.UI.BunifuFlatButton btmInicio;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox Menu;
        private System.Windows.Forms.PictureBox PbLogo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox indicador;
        private Bunifu.Framework.UI.BunifuFlatButton btmDiagnostico;
        private System.Windows.Forms.Panel panelContenedor;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private BunifuAnimatorNS.BunifuTransition PanelAnimacion2;
        private BunifuAnimatorNS.BunifuTransition PanelAnimacion;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}