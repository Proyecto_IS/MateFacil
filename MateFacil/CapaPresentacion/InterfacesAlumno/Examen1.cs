﻿using CapaDatos;
using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Examen1 : Form
    {
        public Examen1()
        {
            InitializeComponent();
        }

        CDConexion conexion = new CDConexion();
        PrincipalAlumno pa = new PrincipalAlumno();
        public String alu = Program.alu;

        public void AbrirFormInPanel(object Formhijo)
        {
            if (this.panel1.Controls.Count > 0)
                this.panel1.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(fh);
            this.panel1.Tag = fh;

            panel2.SendToBack();
            panel3.SendToBack();
            panel4.SendToBack();


            fh.Show();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            VerificarLeccion("L01",1);
        }

        public void VerificarLeccion( String leccion,int num)
        {
            int n = 0;
            SqlCommand comando = new SqlCommand("select count(*) from Avance where Id_Alumno= '" + alu + "' and Puntaje_Ejercicios>0 and Id_Leccion= '"+leccion+"'", conexion.AbrirConexion());
            n = Int32.Parse(Convert.ToString(comando.ExecuteScalar()));
            if (n == 1)
            {
                if (num == 1)
                {
                    Program.bandiagnos = true;
                    AbrirFormInPanel(new Test2());
                   
                }
                if (num == 2)
                {
                    Program.bandiagnos = true;
                    AbrirFormInPanel(new Test1());
                    
                }
                if (num == 3)
                {
                    Program.bandiagnos = true;
                    AbrirFormInPanel(new Test3());
                }
                
            }
            else
            {
                MessageBox.Show("Tienes que leer la lección y realizar los ejercicios para poder hacer el examen.");
            }

        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            VerificarLeccion("L02", 2);
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            VerificarLeccion("L03", 3);
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
