﻿using CapaDatos;
using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Test3 : Form
    {
        public Test3()
        {
            InitializeComponent();
        }

        CDConexion conexion = new CDConexion();
        public String alu = Program.alu;
        public String fecha = "";

        private void button2_Click(object sender, EventArgs e)
        {
            double CalDiag = 0;
            int aciertos = 0;

            if (radioButton2.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton6.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton11.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton15.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton20.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton21.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton27.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton29.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton35.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            if (radioButton37.Checked == true)
            {
                CalDiag += 1;
                aciertos++;
            }
            String compara = "";
            SqlCommand comparar = new SqlCommand("select Calificacion_Examen from Avance where Id_Alumno= '" + alu + "' and Id_Leccion= 'L03'", conexion.AbrirConexion());
            comparar.ExecuteNonQuery();
            compara = Convert.ToString(comparar.ExecuteScalar());
            if (compara.Equals(""))
            {


                MessageBox.Show("Gracias por completar el examen de la lección 1.\nTuviste " + aciertos + " aciertos.\nTu calificación es " + CalDiag);
            int cal = (int)CalDiag;
            if (cal < 6)
            {
                MessageBox.Show("No aprobaste el test, debes intentarlo de nuevo");
            }
            else
            {
                SqlCommand Registrar2 = new SqlCommand("update Avance set Calificacion_Examen=" + cal + " where Id_Alumno='" + alu + "' and Id_Leccion='L03'", conexion.AbrirConexion());
                Registrar2.ExecuteNonQuery();
                ObtenerFecha();
                SqlCommand Registrar3 = new SqlCommand("update Avance set Fecha='" + fecha + "' where Id_Alumno='" + alu + "' and Id_Leccion='L03'", conexion.AbrirConexion());
                Registrar3.ExecuteNonQuery();
                conexion.CerrarConexion();
            }
                
            }
            else
            {
                MessageBox.Show("Tu ya realizaste este examen test previamente tus resultados no se almacenarán.");

            }
            Program.bandiagnos = false;
            AccionBotones();
        }

        public void AccionBotones()
        {

            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            radioButton4.Checked = false;
            radioButton5.Checked = false;
            radioButton6.Checked = false;
            radioButton7.Checked = false;
            radioButton8.Checked = false;
            radioButton9.Checked = false;
            radioButton10.Checked = false;
            radioButton11.Checked = false;
            radioButton12.Checked = false;
            radioButton13.Checked = false;
            radioButton14.Checked = false;
            radioButton15.Checked = false;
            radioButton16.Checked = false;
            radioButton17.Checked = false;
            radioButton18.Checked = false;
            radioButton19.Checked = false;
            radioButton20.Checked = false;
            radioButton21.Checked = false;
            radioButton22.Checked = false;
            radioButton23.Checked = false;
            radioButton24.Checked = false;
            radioButton25.Checked = false;
            radioButton26.Checked = false;
            radioButton27.Checked = false;
            radioButton28.Checked = false;
            radioButton29.Checked = false;
            radioButton30.Checked = false;
            radioButton31.Checked = false;
            radioButton32.Checked = false;
            radioButton33.Checked = false;
            radioButton34.Checked = false;
            radioButton35.Checked = false;
            radioButton36.Checked = false;
            radioButton37.Checked = false;
            radioButton38.Checked = false;
            radioButton39.Checked = false;
            radioButton40.Checked = false;
        }

        public void ObtenerFecha()
        {
            fecha = DateTime.Now.ToShortDateString();
        }

        private void Test3_Load(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
