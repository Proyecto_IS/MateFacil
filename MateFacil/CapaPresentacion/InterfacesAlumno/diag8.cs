﻿using CapaDatos;
using CapaNegocio;
using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class diag8 : Form
    {

        CDConexion conexion = new CDConexion();
        public String alu = Program.alu;
        

        public diag8()
        {
            InitializeComponent();
            Random random = new Random();

                      
        }


        private void button1_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            panel1.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            panel3.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            panel4.Visible = true;
            panel3.Visible = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            panel3.Visible = true;
            panel4.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            panel5.Visible = true;
            panel4.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            panel5.Visible = false;
            panel4.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            panel6.Visible = true;
            panel5.Visible = false;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            panel5.Visible = true;
            panel6.Visible = false;
        }

        private void button11_Click(object sender, EventArgs e)
        {
        
            //panel7.Visible = true;
            panel6.Visible = false;

            double CalDiag = 0;
            int aciertos = 0;

            String r1 = "";
            String r5 = "";
            String r6 = "";
            String r15 = "";

            r1 = (textBox4.Text);
            r5 = (textBox3.Text);
            r6 =(textBox1.Text);
            r15 =(textBox5.Text);

            if (r1.Equals("4") )
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton3.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton10.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton44.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (r5.Equals("17") || r5.Equals("1700"))
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (r6.Equals("4255"))
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton13.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton25.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton20.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton17.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton27.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton6.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton33.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (radioButton39.Checked == true)
            {
                CalDiag += 6.66;
                aciertos++;
            }
            if (r15.Equals("64"))
            {
                CalDiag += 6.66;
                aciertos++;
            }

            String compara = "";

            SqlCommand comparar = new SqlCommand("select Examen_D from Calificaciones where Id_Alumno= '" + alu + "'", conexion.AbrirConexion());
            comparar.ExecuteNonQuery();
            compara = Convert.ToString(comparar.ExecuteScalar());
           
            if (compara.Equals(""))
            {
                MessageBox.Show("Gracias por completar el examen de diagnostico.\nTuviste " + aciertos + " aciertos.\nTu calificación es " + CalDiag);
                int cal = (int)CalDiag;
                SqlCommand Registrar = new SqlCommand("update Calificaciones set Examen_D=" + cal + " where Id_Alumno='" + alu + "'", conexion.AbrirConexion());
                Registrar.ExecuteNonQuery();
                conexion.CerrarConexion();
                this.Close();

            }
            else
            {
                MessageBox.Show("Tu ya realizaste este examen diagnóstico previamente tus resultados no se almacenarán.");

            }

            Program.bandiagnos = false;


        }

        private void button12_Click(object sender, EventArgs e)
        {
            panel3.Visible = true;
            panel2.Visible = false;
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.SoloNumeros(e);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.SoloNumeros(e);
        }
       
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            String a = textBox1.Text;
            Validaciones.NumeroDecimal(e,a);
           
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            String a = textBox5.Text;
            Validaciones.NumeroDecimal(e, a);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void diag8_Load(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
        
}
