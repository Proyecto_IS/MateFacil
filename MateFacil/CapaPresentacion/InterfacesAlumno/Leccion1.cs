﻿using MateFacil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.InterfacesAlumno
{
    public partial class Leccion1 : Form
    {
        public Boolean ban = Program.abrir;
        public Leccion1()
        {
            InitializeComponent();
            
        }

        public void AbrirFormInPanel(object Formhijo)
        {
            
            if (this.panel1.Controls.Count > 0)
                this.panel1.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panel1.Controls.Add(fh);
            this.panel1.Tag = fh;
            fh.Show();

            label2.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            label3.Visible = false;
            this.AutoScroll = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Program.bandiagnos = true;
            AbrirFormInPanel(new Ejercicios1());
          
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
