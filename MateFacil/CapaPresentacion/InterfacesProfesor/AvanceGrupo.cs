﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Windows.Forms.DataVisualization.Charting;
using CapaDatos;
using MateFacil;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class AvanceGrupo : Form
    {
        CDConexion conexion = new CDConexion();
        SqlCommand cmd = new SqlCommand();
        public String cadena = Program.curso;
        public AvanceGrupo()
        {
            InitializeComponent();
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click_1(object sender, EventArgs e)
        {

        }

        private void AvanceGrupo_Load(object sender, EventArgs e)
        {           
            chart1.Series.Clear();
            chart1.Visible = true;

            

            cmd = new SqlCommand("select count(distinct Id_Leccion) from Contenido where Id_Curso='"+cadena+"'", conexion.AbrirConexion());
            Int32 lecciones = Convert.ToInt32(cmd.ExecuteScalar());


            int[] cantAl = new int[4];
           
         
                for (int i = 1; i <= 3 ; i++)
                {
                    cmd = new SqlCommand("select count (*) from Avance,Alumno where Id_Leccion like '%" + i + "' and Avance.Id_Alumno=Alumno.Id_Alumno and Id_Curs='"+cadena+"'", conexion.AbrirConexion());
                    Int32 c = Convert.ToInt32(cmd.ExecuteScalar());
                    cantAl[i] = c;
                }
          
            AvanceGrupo grafica = new AvanceGrupo();

            //este arreglo contiene las lecciones. Van a la derecha de la gráfica
            string[] leccion = new string[4];
            for (int i =1; i <= 3; i++)
            {
                leccion[i] = "Lección " + (i);
            }

            chart1.Palette = ChartColorPalette.Pastel;

            //titulo de la gráfica
            chart1.Titles.Add("Avance Grupal");
            
            for (int i = 1; i <= 3; i++)
            {

                Series serie = chart1.Series.Add(leccion[i]);
                serie.Label = cantAl[i].ToString();
                serie.Points.Add(cantAl[i]);
            }
        }
    }
}
