﻿namespace MateFacil
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation6 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation5 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation4 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Menu = new System.Windows.Forms.PictureBox();
            this.iconminimizar = new System.Windows.Forms.PictureBox();
            this.iconrestaurar = new System.Windows.Forms.PictureBox();
            this.iconmaximizar = new System.Windows.Forms.PictureBox();
            this.iconcerrar = new System.Windows.Forms.PictureBox();
            this.PanelAnimacion = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.MenuVertical = new System.Windows.Forms.Panel();
            this.panelAvance = new System.Windows.Forms.Panel();
            this.btmAvanceIndividual = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmAvanceGrupal = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmBaseDeDatos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmEjecicios = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmLecciones = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmAvances = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAlumnos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btmInicio = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PbLogo = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelContenedor = new System.Windows.Forms.Panel();
            this.PanelAnimacion2 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.PanelTranslacion = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.BarraTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconminimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconrestaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconmaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconcerrar)).BeginInit();
            this.MenuVertical.SuspendLayout();
            this.panelAvance.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(12)))), ((int)(((byte)(14)))));
            this.BarraTitulo.Controls.Add(this.pictureBox1);
            this.BarraTitulo.Controls.Add(this.label3);
            this.BarraTitulo.Controls.Add(this.label1);
            this.BarraTitulo.Controls.Add(this.Menu);
            this.BarraTitulo.Controls.Add(this.iconminimizar);
            this.BarraTitulo.Controls.Add(this.iconrestaurar);
            this.BarraTitulo.Controls.Add(this.iconmaximizar);
            this.BarraTitulo.Controls.Add(this.iconcerrar);
            this.PanelAnimacion.SetDecoration(this.BarraTitulo, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.BarraTitulo, BunifuAnimatorNS.DecorationType.None);
            this.PanelTranslacion.SetDecoration(this.BarraTitulo, BunifuAnimatorNS.DecorationType.None);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.Location = new System.Drawing.Point(0, 0);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(1250, 48);
            this.BarraTitulo.TabIndex = 1;
            this.BarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BarraTitulo_MouseDown);
            this.BarraTitulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.BarraTitulo_MouseMove);
            // 
            // pictureBox1
            // 
            this.PanelTranslacion.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(954, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(37, 29);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(987, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 21);
            this.label3.TabIndex = 12;
            this.label3.Text = "Cerrar Sesión";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.PanelTranslacion.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(56, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 21);
            this.label1.TabIndex = 10;
            this.label1.Text = "Droid PC Desktop";
            // 
            // Menu
            // 
            this.Menu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Menu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.Menu, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.Menu, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.Menu, BunifuAnimatorNS.DecorationType.None);
            this.Menu.Enabled = false;
            this.Menu.Image = ((System.Drawing.Image)(resources.GetObject("Menu.Image")));
            this.Menu.Location = new System.Drawing.Point(242, 1);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(41, 41);
            this.Menu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Menu.TabIndex = 1;
            this.Menu.TabStop = false;
            this.Menu.Visible = false;
            this.Menu.Click += new System.EventHandler(this.Menu_Click_1);
            // 
            // iconminimizar
            // 
            this.iconminimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconminimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.iconminimizar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.iconminimizar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconminimizar, BunifuAnimatorNS.DecorationType.None);
            this.iconminimizar.Image = ((System.Drawing.Image)(resources.GetObject("iconminimizar.Image")));
            this.iconminimizar.Location = new System.Drawing.Point(1182, 3);
            this.iconminimizar.Name = "iconminimizar";
            this.iconminimizar.Size = new System.Drawing.Size(25, 25);
            this.iconminimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconminimizar.TabIndex = 4;
            this.iconminimizar.TabStop = false;
            this.iconminimizar.Click += new System.EventHandler(this.iconminimizar_Click);
            // 
            // iconrestaurar
            // 
            this.iconrestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconrestaurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.iconrestaurar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.iconrestaurar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconrestaurar, BunifuAnimatorNS.DecorationType.None);
            this.iconrestaurar.Enabled = false;
            this.iconrestaurar.Image = ((System.Drawing.Image)(resources.GetObject("iconrestaurar.Image")));
            this.iconrestaurar.Location = new System.Drawing.Point(404, 12);
            this.iconrestaurar.Name = "iconrestaurar";
            this.iconrestaurar.Size = new System.Drawing.Size(25, 25);
            this.iconrestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconrestaurar.TabIndex = 3;
            this.iconrestaurar.TabStop = false;
            this.iconrestaurar.Visible = false;
            this.iconrestaurar.Click += new System.EventHandler(this.iconrestaurar_Click);
            // 
            // iconmaximizar
            // 
            this.iconmaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconmaximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.iconmaximizar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.iconmaximizar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconmaximizar, BunifuAnimatorNS.DecorationType.None);
            this.iconmaximizar.Enabled = false;
            this.iconmaximizar.Image = ((System.Drawing.Image)(resources.GetObject("iconmaximizar.Image")));
            this.iconmaximizar.Location = new System.Drawing.Point(373, 14);
            this.iconmaximizar.Name = "iconmaximizar";
            this.iconmaximizar.Size = new System.Drawing.Size(25, 25);
            this.iconmaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconmaximizar.TabIndex = 2;
            this.iconmaximizar.TabStop = false;
            this.iconmaximizar.Visible = false;
            this.iconmaximizar.Click += new System.EventHandler(this.iconmaximizar_Click);
            // 
            // iconcerrar
            // 
            this.iconcerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconcerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.iconcerrar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.iconcerrar, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.iconcerrar, BunifuAnimatorNS.DecorationType.None);
            this.iconcerrar.Image = ((System.Drawing.Image)(resources.GetObject("iconcerrar.Image")));
            this.iconcerrar.Location = new System.Drawing.Point(1213, 3);
            this.iconcerrar.Name = "iconcerrar";
            this.iconcerrar.Size = new System.Drawing.Size(25, 25);
            this.iconcerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.iconcerrar.TabIndex = 1;
            this.iconcerrar.TabStop = false;
            this.iconcerrar.Click += new System.EventHandler(this.iconcerrar_Click);
            // 
            // PanelAnimacion
            // 
            this.PanelAnimacion.AnimationType = BunifuAnimatorNS.AnimationType.Mosaic;
            this.PanelAnimacion.Cursor = null;
            animation6.AnimateOnlyDifferences = true;
            animation6.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.BlindCoeff")));
            animation6.LeafCoeff = 0F;
            animation6.MaxTime = 1F;
            animation6.MinTime = 0F;
            animation6.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.MosaicCoeff")));
            animation6.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation6.MosaicShift")));
            animation6.MosaicSize = 20;
            animation6.Padding = new System.Windows.Forms.Padding(30);
            animation6.RotateCoeff = 0F;
            animation6.RotateLimit = 0F;
            animation6.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.ScaleCoeff")));
            animation6.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.SlideCoeff")));
            animation6.TimeCoeff = 0F;
            animation6.TransparencyCoeff = 0F;
            this.PanelAnimacion.DefaultAnimation = animation6;
            // 
            // MenuVertical
            // 
            this.MenuVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.MenuVertical.Controls.Add(this.panelAvance);
            this.MenuVertical.Controls.Add(this.btmBaseDeDatos);
            this.MenuVertical.Controls.Add(this.btmEjecicios);
            this.MenuVertical.Controls.Add(this.btmLecciones);
            this.MenuVertical.Controls.Add(this.btmAvances);
            this.MenuVertical.Controls.Add(this.btnAlumnos);
            this.MenuVertical.Controls.Add(this.btmInicio);
            this.MenuVertical.Controls.Add(this.panel1);
            this.PanelAnimacion.SetDecoration(this.MenuVertical, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.MenuVertical, BunifuAnimatorNS.DecorationType.None);
            this.PanelTranslacion.SetDecoration(this.MenuVertical, BunifuAnimatorNS.DecorationType.None);
            this.MenuVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuVertical.Location = new System.Drawing.Point(0, 48);
            this.MenuVertical.Name = "MenuVertical";
            this.MenuVertical.Size = new System.Drawing.Size(283, 602);
            this.MenuVertical.TabIndex = 0;
            // 
            // panelAvance
            // 
            this.panelAvance.Controls.Add(this.btmAvanceIndividual);
            this.panelAvance.Controls.Add(this.btmAvanceGrupal);
            this.PanelAnimacion.SetDecoration(this.panelAvance, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.panelAvance, BunifuAnimatorNS.DecorationType.None);
            this.PanelTranslacion.SetDecoration(this.panelAvance, BunifuAnimatorNS.DecorationType.None);
            this.panelAvance.Location = new System.Drawing.Point(274, 203);
            this.panelAvance.Name = "panelAvance";
            this.panelAvance.Size = new System.Drawing.Size(242, 105);
            this.panelAvance.TabIndex = 4;
            this.panelAvance.Visible = false;
            // 
            // btmAvanceIndividual
            // 
            this.btmAvanceIndividual.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmAvanceIndividual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmAvanceIndividual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmAvanceIndividual.BorderRadius = 0;
            this.btmAvanceIndividual.ButtonText = "             Avance individual";
            this.btmAvanceIndividual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btmAvanceIndividual, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmAvanceIndividual, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btmAvanceIndividual, BunifuAnimatorNS.DecorationType.None);
            this.btmAvanceIndividual.DisabledColor = System.Drawing.Color.Gray;
            this.btmAvanceIndividual.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvanceIndividual.Iconcolor = System.Drawing.Color.Transparent;
            this.btmAvanceIndividual.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmAvanceIndividual.Iconimage")));
            this.btmAvanceIndividual.Iconimage_right = null;
            this.btmAvanceIndividual.Iconimage_right_Selected = null;
            this.btmAvanceIndividual.Iconimage_Selected = null;
            this.btmAvanceIndividual.IconMarginLeft = 20;
            this.btmAvanceIndividual.IconMarginRight = 0;
            this.btmAvanceIndividual.IconRightVisible = true;
            this.btmAvanceIndividual.IconRightZoom = 0D;
            this.btmAvanceIndividual.IconVisible = true;
            this.btmAvanceIndividual.IconZoom = 50D;
            this.btmAvanceIndividual.IsTab = true;
            this.btmAvanceIndividual.Location = new System.Drawing.Point(0, 56);
            this.btmAvanceIndividual.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmAvanceIndividual.Name = "btmAvanceIndividual";
            this.btmAvanceIndividual.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmAvanceIndividual.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmAvanceIndividual.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmAvanceIndividual.selected = false;
            this.btmAvanceIndividual.Size = new System.Drawing.Size(241, 49);
            this.btmAvanceIndividual.TabIndex = 3;
            this.btmAvanceIndividual.Text = "             Avance individual";
            this.btmAvanceIndividual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmAvanceIndividual.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmAvanceIndividual.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvanceIndividual.Click += new System.EventHandler(this.btmAvanceIndividual_Click_1);
            // 
            // btmAvanceGrupal
            // 
            this.btmAvanceGrupal.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmAvanceGrupal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmAvanceGrupal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmAvanceGrupal.BorderRadius = 0;
            this.btmAvanceGrupal.ButtonText = "             Avance Grupal";
            this.btmAvanceGrupal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btmAvanceGrupal, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmAvanceGrupal, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btmAvanceGrupal, BunifuAnimatorNS.DecorationType.None);
            this.btmAvanceGrupal.DisabledColor = System.Drawing.Color.Gray;
            this.btmAvanceGrupal.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvanceGrupal.Iconcolor = System.Drawing.Color.Transparent;
            this.btmAvanceGrupal.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmAvanceGrupal.Iconimage")));
            this.btmAvanceGrupal.Iconimage_right = null;
            this.btmAvanceGrupal.Iconimage_right_Selected = null;
            this.btmAvanceGrupal.Iconimage_Selected = null;
            this.btmAvanceGrupal.IconMarginLeft = 20;
            this.btmAvanceGrupal.IconMarginRight = 0;
            this.btmAvanceGrupal.IconRightVisible = true;
            this.btmAvanceGrupal.IconRightZoom = 0D;
            this.btmAvanceGrupal.IconVisible = true;
            this.btmAvanceGrupal.IconZoom = 50D;
            this.btmAvanceGrupal.IsTab = true;
            this.btmAvanceGrupal.Location = new System.Drawing.Point(0, 4);
            this.btmAvanceGrupal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmAvanceGrupal.Name = "btmAvanceGrupal";
            this.btmAvanceGrupal.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmAvanceGrupal.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmAvanceGrupal.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmAvanceGrupal.selected = false;
            this.btmAvanceGrupal.Size = new System.Drawing.Size(241, 44);
            this.btmAvanceGrupal.TabIndex = 3;
            this.btmAvanceGrupal.Text = "             Avance Grupal";
            this.btmAvanceGrupal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmAvanceGrupal.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmAvanceGrupal.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvanceGrupal.Click += new System.EventHandler(this.btmAvanceGrupal_Click_1);
            // 
            // btmBaseDeDatos
            // 
            this.btmBaseDeDatos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmBaseDeDatos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmBaseDeDatos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmBaseDeDatos.BorderRadius = 0;
            this.btmBaseDeDatos.ButtonText = "      Base de datos";
            this.btmBaseDeDatos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btmBaseDeDatos, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmBaseDeDatos, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btmBaseDeDatos, BunifuAnimatorNS.DecorationType.None);
            this.btmBaseDeDatos.DisabledColor = System.Drawing.Color.Gray;
            this.btmBaseDeDatos.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmBaseDeDatos.Iconcolor = System.Drawing.Color.Transparent;
            this.btmBaseDeDatos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmBaseDeDatos.Iconimage")));
            this.btmBaseDeDatos.Iconimage_right = null;
            this.btmBaseDeDatos.Iconimage_right_Selected = null;
            this.btmBaseDeDatos.Iconimage_Selected = null;
            this.btmBaseDeDatos.IconMarginLeft = 20;
            this.btmBaseDeDatos.IconMarginRight = 0;
            this.btmBaseDeDatos.IconRightVisible = true;
            this.btmBaseDeDatos.IconRightZoom = 0D;
            this.btmBaseDeDatos.IconVisible = true;
            this.btmBaseDeDatos.IconZoom = 80D;
            this.btmBaseDeDatos.IsTab = true;
            this.btmBaseDeDatos.Location = new System.Drawing.Point(0, 539);
            this.btmBaseDeDatos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmBaseDeDatos.Name = "btmBaseDeDatos";
            this.btmBaseDeDatos.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmBaseDeDatos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmBaseDeDatos.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmBaseDeDatos.selected = false;
            this.btmBaseDeDatos.Size = new System.Drawing.Size(280, 50);
            this.btmBaseDeDatos.TabIndex = 3;
            this.btmBaseDeDatos.Text = "      Base de datos";
            this.btmBaseDeDatos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmBaseDeDatos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmBaseDeDatos.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmBaseDeDatos.Click += new System.EventHandler(this.btmBaseDeDatos_Click);
            // 
            // btmEjecicios
            // 
            this.btmEjecicios.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmEjecicios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmEjecicios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmEjecicios.BorderRadius = 0;
            this.btmEjecicios.ButtonText = "      Ejercicios";
            this.btmEjecicios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btmEjecicios, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmEjecicios, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btmEjecicios, BunifuAnimatorNS.DecorationType.None);
            this.btmEjecicios.DisabledColor = System.Drawing.Color.Gray;
            this.btmEjecicios.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmEjecicios.Iconcolor = System.Drawing.Color.Transparent;
            this.btmEjecicios.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmEjecicios.Iconimage")));
            this.btmEjecicios.Iconimage_right = null;
            this.btmEjecicios.Iconimage_right_Selected = null;
            this.btmEjecicios.Iconimage_Selected = null;
            this.btmEjecicios.IconMarginLeft = 20;
            this.btmEjecicios.IconMarginRight = 0;
            this.btmEjecicios.IconRightVisible = true;
            this.btmEjecicios.IconRightZoom = 0D;
            this.btmEjecicios.IconVisible = true;
            this.btmEjecicios.IconZoom = 80D;
            this.btmEjecicios.IsTab = true;
            this.btmEjecicios.Location = new System.Drawing.Point(4, 475);
            this.btmEjecicios.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmEjecicios.Name = "btmEjecicios";
            this.btmEjecicios.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmEjecicios.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmEjecicios.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmEjecicios.selected = false;
            this.btmEjecicios.Size = new System.Drawing.Size(280, 50);
            this.btmEjecicios.TabIndex = 3;
            this.btmEjecicios.Text = "      Ejercicios";
            this.btmEjecicios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmEjecicios.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmEjecicios.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmEjecicios.Click += new System.EventHandler(this.btmEjecicios_Click_1);
            // 
            // btmLecciones
            // 
            this.btmLecciones.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmLecciones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmLecciones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmLecciones.BorderRadius = 0;
            this.btmLecciones.ButtonText = "      Lecciones";
            this.btmLecciones.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btmLecciones, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmLecciones, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btmLecciones, BunifuAnimatorNS.DecorationType.None);
            this.btmLecciones.DisabledColor = System.Drawing.Color.Gray;
            this.btmLecciones.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmLecciones.Iconcolor = System.Drawing.Color.Transparent;
            this.btmLecciones.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmLecciones.Iconimage")));
            this.btmLecciones.Iconimage_right = null;
            this.btmLecciones.Iconimage_right_Selected = null;
            this.btmLecciones.Iconimage_Selected = null;
            this.btmLecciones.IconMarginLeft = 20;
            this.btmLecciones.IconMarginRight = 0;
            this.btmLecciones.IconRightVisible = true;
            this.btmLecciones.IconRightZoom = 0D;
            this.btmLecciones.IconVisible = true;
            this.btmLecciones.IconZoom = 80D;
            this.btmLecciones.IsTab = true;
            this.btmLecciones.Location = new System.Drawing.Point(0, 406);
            this.btmLecciones.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmLecciones.Name = "btmLecciones";
            this.btmLecciones.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmLecciones.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmLecciones.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmLecciones.selected = false;
            this.btmLecciones.Size = new System.Drawing.Size(280, 50);
            this.btmLecciones.TabIndex = 3;
            this.btmLecciones.Text = "      Lecciones";
            this.btmLecciones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmLecciones.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmLecciones.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmLecciones.Click += new System.EventHandler(this.btmLecciones_Click_1);
            // 
            // btmAvances
            // 
            this.btmAvances.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmAvances.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmAvances.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmAvances.BorderRadius = 0;
            this.btmAvances.ButtonText = "      Avances";
            this.btmAvances.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btmAvances, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmAvances, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btmAvances, BunifuAnimatorNS.DecorationType.None);
            this.btmAvances.DisabledColor = System.Drawing.Color.Gray;
            this.btmAvances.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvances.Iconcolor = System.Drawing.Color.Transparent;
            this.btmAvances.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmAvances.Iconimage")));
            this.btmAvances.Iconimage_right = null;
            this.btmAvances.Iconimage_right_Selected = null;
            this.btmAvances.Iconimage_Selected = null;
            this.btmAvances.IconMarginLeft = 20;
            this.btmAvances.IconMarginRight = 0;
            this.btmAvances.IconRightVisible = true;
            this.btmAvances.IconRightZoom = 0D;
            this.btmAvances.IconVisible = true;
            this.btmAvances.IconZoom = 80D;
            this.btmAvances.IsTab = true;
            this.btmAvances.Location = new System.Drawing.Point(0, 339);
            this.btmAvances.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmAvances.Name = "btmAvances";
            this.btmAvances.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmAvances.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmAvances.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmAvances.selected = false;
            this.btmAvances.Size = new System.Drawing.Size(283, 50);
            this.btmAvances.TabIndex = 3;
            this.btmAvances.Text = "      Avances";
            this.btmAvances.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmAvances.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmAvances.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvances.Click += new System.EventHandler(this.btmAvances_Click_1);
            // 
            // btnAlumnos
            // 
            this.btnAlumnos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnAlumnos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btnAlumnos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAlumnos.BorderRadius = 0;
            this.btnAlumnos.ButtonText = "      Alumnos";
            this.btnAlumnos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btnAlumnos, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btnAlumnos, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btnAlumnos, BunifuAnimatorNS.DecorationType.None);
            this.btnAlumnos.DisabledColor = System.Drawing.Color.Gray;
            this.btnAlumnos.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlumnos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAlumnos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAlumnos.Iconimage")));
            this.btnAlumnos.Iconimage_right = null;
            this.btnAlumnos.Iconimage_right_Selected = null;
            this.btnAlumnos.Iconimage_Selected = null;
            this.btnAlumnos.IconMarginLeft = 20;
            this.btnAlumnos.IconMarginRight = 0;
            this.btnAlumnos.IconRightVisible = true;
            this.btnAlumnos.IconRightZoom = 0D;
            this.btnAlumnos.IconVisible = true;
            this.btnAlumnos.IconZoom = 80D;
            this.btnAlumnos.IsTab = true;
            this.btnAlumnos.Location = new System.Drawing.Point(0, 272);
            this.btnAlumnos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAlumnos.Name = "btnAlumnos";
            this.btnAlumnos.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btnAlumnos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnAlumnos.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btnAlumnos.selected = false;
            this.btnAlumnos.Size = new System.Drawing.Size(280, 50);
            this.btnAlumnos.TabIndex = 3;
            this.btnAlumnos.Text = "      Alumnos";
            this.btnAlumnos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlumnos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btnAlumnos.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlumnos.Click += new System.EventHandler(this.btnAlumnos_Click_1);
            // 
            // btmInicio
            // 
            this.btmInicio.Activecolor = System.Drawing.Color.Gainsboro;
            this.btmInicio.BackColor = System.Drawing.Color.Gainsboro;
            this.btmInicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btmInicio.BorderRadius = 0;
            this.btmInicio.ButtonText = "      Inicio";
            this.btmInicio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PanelTranslacion.SetDecoration(this.btmInicio, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.btmInicio, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.btmInicio, BunifuAnimatorNS.DecorationType.None);
            this.btmInicio.DisabledColor = System.Drawing.Color.Gray;
            this.btmInicio.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmInicio.Iconcolor = System.Drawing.Color.Transparent;
            this.btmInicio.Iconimage = ((System.Drawing.Image)(resources.GetObject("btmInicio.Iconimage")));
            this.btmInicio.Iconimage_right = null;
            this.btmInicio.Iconimage_right_Selected = null;
            this.btmInicio.Iconimage_Selected = null;
            this.btmInicio.IconMarginLeft = 20;
            this.btmInicio.IconMarginRight = 0;
            this.btmInicio.IconRightVisible = true;
            this.btmInicio.IconRightZoom = 0D;
            this.btmInicio.IconVisible = true;
            this.btmInicio.IconZoom = 80D;
            this.btmInicio.IsTab = true;
            this.btmInicio.Location = new System.Drawing.Point(3, 204);
            this.btmInicio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btmInicio.Name = "btmInicio";
            this.btmInicio.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btmInicio.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btmInicio.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmInicio.selected = true;
            this.btmInicio.Size = new System.Drawing.Size(280, 50);
            this.btmInicio.TabIndex = 3;
            this.btmInicio.Text = "      Inicio";
            this.btmInicio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmInicio.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(42)))), ((int)(((byte)(53)))));
            this.btmInicio.TextFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmInicio.Click += new System.EventHandler(this.btmInicio_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.panel1.Controls.Add(this.PbLogo);
            this.panel1.Controls.Add(this.label2);
            this.PanelAnimacion.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.PanelTranslacion.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 197);
            this.panel1.TabIndex = 1;
            // 
            // PbLogo
            // 
            this.PanelTranslacion.SetDecoration(this.PbLogo, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.PbLogo, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.PbLogo, BunifuAnimatorNS.DecorationType.None);
            this.PbLogo.Image = ((System.Drawing.Image)(resources.GetObject("PbLogo.Image")));
            this.PbLogo.Location = new System.Drawing.Point(60, 0);
            this.PbLogo.Name = "PbLogo";
            this.PbLogo.Size = new System.Drawing.Size(187, 158);
            this.PbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PbLogo.TabIndex = 1;
            this.PbLogo.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.PanelTranslacion.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(90, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 22);
            this.label2.TabIndex = 10;
            this.label2.Text = "MateFacil-PC";
            // 
            // panelContenedor
            // 
            this.PanelAnimacion.SetDecoration(this.panelContenedor, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion2.SetDecoration(this.panelContenedor, BunifuAnimatorNS.DecorationType.None);
            this.PanelTranslacion.SetDecoration(this.panelContenedor, BunifuAnimatorNS.DecorationType.None);
            this.panelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenedor.Location = new System.Drawing.Point(283, 48);
            this.panelContenedor.Name = "panelContenedor";
            this.panelContenedor.Size = new System.Drawing.Size(967, 602);
            this.panelContenedor.TabIndex = 2;
            this.panelContenedor.Paint += new System.Windows.Forms.PaintEventHandler(this.panelContenedor_Paint);
            // 
            // PanelAnimacion2
            // 
            this.PanelAnimacion2.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.PanelAnimacion2.Cursor = null;
            animation5.AnimateOnlyDifferences = true;
            animation5.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.BlindCoeff")));
            animation5.LeafCoeff = 0F;
            animation5.MaxTime = 1F;
            animation5.MinTime = 0F;
            animation5.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.MosaicCoeff")));
            animation5.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation5.MosaicShift")));
            animation5.MosaicSize = 0;
            animation5.Padding = new System.Windows.Forms.Padding(0);
            animation5.RotateCoeff = 0F;
            animation5.RotateLimit = 0F;
            animation5.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.ScaleCoeff")));
            animation5.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.SlideCoeff")));
            animation5.TimeCoeff = 0F;
            animation5.TransparencyCoeff = 0F;
            this.PanelAnimacion2.DefaultAnimation = animation5;
            // 
            // PanelTranslacion
            // 
            this.PanelTranslacion.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.PanelTranslacion.Cursor = null;
            animation4.AnimateOnlyDifferences = true;
            animation4.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.BlindCoeff")));
            animation4.LeafCoeff = 0F;
            animation4.MaxTime = 1F;
            animation4.MinTime = 0F;
            animation4.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.MosaicCoeff")));
            animation4.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation4.MosaicShift")));
            animation4.MosaicSize = 0;
            animation4.Padding = new System.Windows.Forms.Padding(0);
            animation4.RotateCoeff = 0F;
            animation4.RotateLimit = 0F;
            animation4.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.ScaleCoeff")));
            animation4.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.SlideCoeff")));
            animation4.TimeCoeff = 0F;
            animation4.TransparencyCoeff = 0F;
            this.PanelTranslacion.DefaultAnimation = animation4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1250, 650);
            this.Controls.Add(this.panelContenedor);
            this.Controls.Add(this.MenuVertical);
            this.Controls.Add(this.BarraTitulo);
            this.PanelAnimacion2.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.PanelAnimacion.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.PanelTranslacion.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.BarraTitulo.ResumeLayout(false);
            this.BarraTitulo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconminimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconrestaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconmaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconcerrar)).EndInit();
            this.MenuVertical.ResumeLayout(false);
            this.panelAvance.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel BarraTitulo;
        private System.Windows.Forms.PictureBox iconminimizar;
        private System.Windows.Forms.PictureBox iconrestaurar;
        private System.Windows.Forms.PictureBox iconmaximizar;
        private System.Windows.Forms.PictureBox iconcerrar;
        private BunifuAnimatorNS.BunifuTransition PanelAnimacion;
        private BunifuAnimatorNS.BunifuTransition PanelAnimacion2;
        private System.Windows.Forms.Panel MenuVertical;
        private System.Windows.Forms.Panel panelContenedor;
        private new System.Windows.Forms.PictureBox Menu;
        private BunifuAnimatorNS.BunifuTransition PanelTranslacion;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox PbLogo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuFlatButton btmInicio;
        private Bunifu.Framework.UI.BunifuFlatButton btmBaseDeDatos;
        private Bunifu.Framework.UI.BunifuFlatButton btmEjecicios;
        private Bunifu.Framework.UI.BunifuFlatButton btmLecciones;
        private Bunifu.Framework.UI.BunifuFlatButton btmAvances;
        private Bunifu.Framework.UI.BunifuFlatButton btnAlumnos;
        private System.Windows.Forms.Panel panelAvance;
        private Bunifu.Framework.UI.BunifuFlatButton btmAvanceIndividual;
        private Bunifu.Framework.UI.BunifuFlatButton btmAvanceGrupal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

