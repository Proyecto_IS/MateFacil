﻿using System;
using System.Drawing;
using CapaNegocio;
using CapaDatos;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace MateFacil
{
    public partial class RegistrarProfesor : Form
    {
        CDConexion conexion = new CDConexion();

        public RegistrarProfesor()
        {
            InitializeComponent();
        }

        public String GenerarCodigoProfesor()
        {
            String codigo = "";
            SqlCommand comando = new SqlCommand("SELECT MAX(Id_Profesor) FROM Profesor", conexion.AbrirConexion());
            codigo = Convert.ToString(comando.ExecuteScalar());

            if (codigo.Equals(""))
            {
                codigo = "P0001";
            }
            else
            {
                String nuevo = "";

                for (int i = 1; i < 5; i++)
                {
                    nuevo += codigo[i];
                }

                int num = Convert.ToInt32(nuevo);
                num++;
                nuevo = Convert.ToString(num);
                if (nuevo.Length == 1)
                {
                    codigo = "P000" + nuevo;
                }
                else if (nuevo.Length == 2)
                {
                    codigo = "P00" + nuevo;
                }
                else if (nuevo.Length == 3)
                {
                    codigo = "P0" + nuevo;
                }
                else if (nuevo.Length == 4)
                {
                    codigo = "P" + nuevo;
                }

               
            }

            return codigo;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btmRegistrar_Click(object sender, EventArgs e)
        {
            CNUsuario objRegistrar = new CNUsuario();
            CDUsuarios sql = new CDUsuarios();
            Validaciones objValidar = new Validaciones();

            SqlDataReader ValidarEmail;
            SqlDataReader ValidarUsuario;

            objRegistrar.IDUsuario = txtIdUsuario.Text;
            objRegistrar.Password = txtPassword.Text;
            objRegistrar.NombreProfesor = txtNombrePro.Text;
            objRegistrar.ApellidoProfesor = txtApellidoPro.Text;
            objRegistrar.CorreoProfesor = txtCorreoPro.Text;

            String ID = GenerarCodigoProfesor();

            try
            {
                if (objRegistrar.IDUsuario == txtIdUsuario.Text)
                {
                    lblIdUsuario.Visible = false;

                    if (objRegistrar.Password == txtPassword.Text)
                    {
                        lblpass.Visible = false;

                        if (objRegistrar.NombreProfesor == txtNombrePro.Text)
                        {
                            lblNombre.Visible = false;

                            if (objRegistrar.ApellidoProfesor == txtApellidoPro.Text)
                            {
                                lblApellido.Visible = false;

                                if (objRegistrar.CorreoProfesor == txtCorreoPro.Text)
                                {
                                    lblCorreo.Visible = false;

                                    if (objValidar.ValirCorreo(txtCorreoPro.Text))
                                    {
                                        ValidarUsuario = objRegistrar.ConfirmarUsuario();

                                        if (ValidarUsuario.Read() == true)
                                        {
                                            lblIdUsuario.Text = "El Usuario ingresado esta siendo utilizado";
                                            lblIdUsuario.ForeColor = Color.Red;
                                            lblIdUsuario.Visible = true;
                                        }
                                        else
                                        {

                                            ValidarEmail = objRegistrar.ConfirmarEmail();

                                            if (ValidarEmail.Read() == true)
                                            {
                                                lblCorreo.Text = "El correo ingresado esta siendo utilizado";
                                                lblCorreo.ForeColor = Color.Red;
                                                lblCorreo.Visible = true;
                                            }
                                            else
                                            {

                                                if (sql.RegistrarMaestro(txtIdUsuario.Text, txtPassword.Text, txtNombrePro.Text, txtApellidoPro.Text, txtCorreoPro.Text,ID))
                                                {
                                                    lblDatosAlmacenados.Text = "Datos almacenados Correctamente";
                                                    lblDatosAlmacenados.Visible = true;
                                                    txtIdUsuario.Text = "";
                                                    txtPassword.Text = "";
                                                    txtNombrePro.Text = "";
                                                    txtApellidoPro.Text = "";
                                                    txtCorreoPro.Text = "";
                                                    this.Close();
                                                }

                                                else
                                                {
                                                    lblDatosAlmacenados.Text = "Error de almacenamiento de datos";
                                                    lblDatosAlmacenados.Visible = true;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        lblCorreo.Text = "Correo invalido";
                                        lblCorreo.ForeColor = Color.Red;
                                        lblCorreo.Visible = true;
                                    }
                                }
                                else
                                {
                                    lblCorreo.Text = objRegistrar.CorreoProfesor;
                                    lblCorreo.Visible = true;
                                }
                            }
                            else
                            {
                                lblApellido.Text = objRegistrar.ApellidoProfesor;
                                lblApellido.Visible = true;
                            }
                        }
                        else
                        {
                            lblNombre.Text = objRegistrar.NombreProfesor;
                            lblNombre.Visible = true;
                        }
                    }
                    else
                    {
                        lblpass.Text = objRegistrar.Password;
                        lblpass.Visible = true;
                    }
                }
                else
                {
                    lblIdUsuario.Text = objRegistrar.IDUsuario;
                    lblIdUsuario.Visible = true;
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Error excepcion " + ex);
            }
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {

            if (txtPassword.Text == "Ingrese Contraseña")
            {
                txtPassword.Text = "";
                txtPassword.ForeColor = Color.LightGray;
                txtPassword.isPassword = false;
            }
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text == "")
            {
                txtPassword.Text = "Ingrese Contraseña";
                txtPassword.ForeColor = Color.DimGray;
                txtPassword.isPassword = true;

            }
        }

        private void txtIdUsuario_Leave(object sender, EventArgs e)
        {
            if (txtIdUsuario.Text == "")
            {
                txtIdUsuario.Text = "Usuario";
                txtIdUsuario.ForeColor = Color.DimGray;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        Point lastPoint;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void txtIdUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.ValidarNombreUsuario(e);
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validaciones.ValidaAgregarAlumno(e);
            if(e.KeyChar <=90 && e.KeyChar >= 65 || e.KeyChar <= 122 && e.KeyChar >= 97)
            {
                e.Handled = false;
                txtPassword.ForeColor = Color.LightGray;
            }
            else if(e.KeyChar <= 57 && e.KeyChar >= 49)
            {
                e.Handled = false;
                txtPassword.ForeColor = Color.LightGray;
            }
            else if(e.KeyChar == 08)
            {
                e.Handled = false;
                txtPassword.ForeColor = Color.LightGray;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtNombrePro_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.ValidaAgregarAlumno(e);
        }

        private void txtApellidoPro_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.ValidaAgregarAlumno(e);
        }

        private void txtCorreoPro_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validaciones.ArrobaYPunto(e);
        }

        private void txtIdUsuario_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void RegistrarProfesor_Load(object sender, EventArgs e)
        {
 
        }

        private void txtCorreoPro_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void txtPassword_MouseEnter(object sender, EventArgs e)
        {
            /*if(txtPassword.Text == "Contraseña")
            {
                txtPassword.ForeColor = Color.LightGray;
                txtPassword.Text = "";
                txtPassword.isPassword = false;
            }*/
        }

        private void txtPassword_MouseLeave(object sender, EventArgs e)
        {
            /*if(txtPassword.Text == "")
            {
                txtPassword.ForeColor = Color.LightGray;
                txtPassword.Text = "Contraseña";
                txtPassword.isPassword = true;
            }*/
        }
    }
}
