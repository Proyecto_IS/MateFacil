﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using CapaDatos;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class BaseDeDatos : Form
    {
        CDConexion conexion = new CDConexion();
        public BaseDeDatos()
        {
            InitializeComponent();
        }

        private void btmUbicacionCopia_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtBackup.Text = dlg.SelectedPath;
                //btmUbicacionCopia.Enabled = true;
            }
        }

        private void btmCopiaBD_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBackup.Text == string.Empty)
                {
                    MessageBox.Show("Ingrese la ubicacion donde se hara la restauracion del base de datos");
                    
                }
                else
                {
                    String comando_consulta = "BACKUP DATABASE [MateFacil] TO  DISK = '" + txtBackup.Text + "\\" + "Hoy" + "-" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".bak'";
                    SqlCommand cmd = new SqlCommand(comando_consulta, conexion.AbrirConexion());

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("La copia de seguridad se realizo correctamente","",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " +ex);
            }
            finally
            {
                conexion.CerrarConexion();
            }
        }

        private void btmUbicacionRes_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "SQL SERVER database bakup files|*.bak";
            ofd.Title = "Database Restore";
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                txtRestore.Text = ofd.FileName;    
            }
        }

        private void btmResBD_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtRestore.Text == string.Empty)
                {
                    MessageBox.Show("Elija la base de datos que decea restaurar");
                }
                else
                {
                    string sql1 = string.Format("ALTER DATABASE [MateFacil] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
                    SqlCommand cmd1 = new SqlCommand(sql1, conexion.AbrirConexion());
                    cmd1.ExecuteNonQuery();

                    String sql2 = string.Format("USE MASTER RESTORE DATABASE [MateFacil] FROM DISK ='" + txtRestore.Text + "' WITH REPLACE;");
                    SqlCommand cmd2 = new SqlCommand(sql2, conexion.AbrirConexion());
                    cmd2.ExecuteNonQuery();

                    string sql3 = string.Format("ALTER DATABASE [MateFacil] SET MULTI_USER");
                    SqlCommand cmd3 = new SqlCommand(sql3, conexion.AbrirConexion());
                    cmd3.ExecuteNonQuery();

                    MessageBox.Show("La base de datos fue restaurada", "", MessageBoxButtons.OK);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR: " + ex);
            }
        }
    }
}
