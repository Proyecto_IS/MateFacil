﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CapaNegocio;
using System.Data.SqlClient;
using CapaDatos;

namespace MateFacil
{
    public partial class IngresarProfesor : Form
    {
        CDConexion conexion = new CDConexion();
        public Boolean ban = Program.abrir;
        //sMateFacil.IngresarProfesor objClase = new MateFacil.IngresarProfesor();
        public void ObtenerCurso(String usuario)
        {
           
        SqlCommand comando = new SqlCommand("ObtenerCurso '"+usuario+"'", conexion.AbrirConexion());
        Program.curso = Convert.ToString(comando.ExecuteScalar());
             
    }

        public IngresarProfesor()
        {
            InitializeComponent();
        }

        private void barraDeTituloLogin_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        Point lastPoint;

        private void barraDeTituloLogin_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btmLogin_Click(object sender, EventArgs e)
        {
            CNUsuario objUsuario = new CNUsuario();
            SqlDataReader loguear;
            objUsuario.Usuario = txtUsuario.Text;
            objUsuario.Contraseña = Contrasena.Text;
            string usuario = txtUsuario.Text;
           

            try
            {
                if (objUsuario.Usuario == txtUsuario.Text)
                {
                    lblErrorUsuario.Visible = false;
                    lblErrorLogin.Visible = false;

                    if (objUsuario.Contraseña == Contrasena.Text)
                    {
                        lblErrorPass.Visible = false;
                        lblErrorLogin.Visible = false;

                        loguear = objUsuario.IniciarSesion();
                        if (loguear.Read() == true)
                        {
                            ObtenerCurso(usuario);
                            this.Hide();
                            Form1 objFP = new Form1();
                            //CapaPresentacion.Cargar car = new CapaPresentacion.Cargar();
                            CapaPresentacion.InterfacesAlumno.PrincipalAlumno objFP2 = new CapaPresentacion.InterfacesAlumno.PrincipalAlumno();
                            CapaPresentacion.InterfacesAuxiliares.AvisoLogin objAVISO = new CapaPresentacion.InterfacesAuxiliares.AvisoLogin();
                            if (usuario[0].Equals('A') && usuario[1].Equals('0'))
                            {
                                Program.alu = usuario;
                                objFP2.Show();
                                ban = true;
                                objAVISO.label1.Text = "Usted a ingresado como alumno";
                                objAVISO.ShowDialog();
                            }
                            else
                            {
                                objFP.Show();
                                objAVISO.ShowDialog();
                            }
                            
                        }
                        else
                        {
                            lblErrorLogin.Text = ("Usuario o Contraseña Ivalido, Intente de nuevo");
                            lblErrorLogin.Visible = true;
                            Contrasena.Text = "";
                            //Contrasena_Leave(null, e);
                            txtUsuario.Focus();
                        }
                    }
                    else
                    {
                        lblErrorPass.Text = objUsuario.Contraseña;
                        lblErrorPass.Visible = true;
                    }

                }
                else
                {
                    lblErrorUsuario.Text = objUsuario.Usuario;
                    lblErrorUsuario.Visible = true;
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Error excepcion " + ex);
            }
        }

        private void lblRegistrarse_Click(object sender, EventArgs e)
        {
            ProCodigoRegistro objCodigoPro = new ProCodigoRegistro();
            objCodigoPro.Show();
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.ValidarNombreUsuario(e);
        }

        private void Contrasena_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.LetrasYNumeros(e);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RecuperaPass_Click(object sender, EventArgs e)
        {
            CapaPresentacion.InterfacesProfesor.RecuperarContraseña form = new CapaPresentacion.InterfacesProfesor.RecuperarContraseña();
            form.ShowDialog();
        }

        private void Contrasena_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void Contrasena_MouseEnter(object sender, EventArgs e)
        {
            
        }

        private void Contrasena_MouseLeave(object sender, EventArgs e)
        {

        }

        private void Contrasena_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 90 && e.KeyChar >= 65 || e.KeyChar <= 122 && e.KeyChar >= 97)
            {
                e.Handled = false;
                Contrasena.isPassword = true;
                Contrasena.ForeColor = Color.LightGray;
            }
            else if (e.KeyChar <= 57 && e.KeyChar >= 48)
            {
                e.Handled = false;
                Contrasena.isPassword = true;
                Contrasena.ForeColor = Color.LightGray;
            }
            else if (e.KeyChar == 08)
            {
                e.Handled = false;
                Contrasena.isPassword = true;
                Contrasena.ForeColor = Color.LightGray;
            }
            else if(e.KeyChar == 13)
            {
                btmLogin_Click(sender,e);
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
