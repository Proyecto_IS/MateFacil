﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CapaPresentacion
{
    public partial class Grafica : Form
    {
        public Grafica()
        {
            InitializeComponent();
        }

        private void chart1_Click(object sender, EventArgs e)
        {
            //vectores con los datos
            string[] series = { "nombre", "apellido" };
            int[] calif = { 10, 20, 30 };

            //cambiar colores
            chart1.Palette = ChartColorPalette.Berry;

            chart1.Titles.Add("Calificaciones");

            for (int i = 0; i < series.Length; i++)
            {
                //titulos
                Series serie = chart1.Series.Add(series[i]);

                //cantidades
                serie.Label = calif[i].ToString();

                serie.Points.Add(calif[i]);
            }
        }

        private void chart2_Click(object sender, EventArgs e)
        {

        }
    }
}
