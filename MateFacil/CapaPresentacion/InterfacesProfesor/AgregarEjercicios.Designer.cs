﻿namespace CapaPresentacion.InterfacesProfesor
{
    partial class AgregarEjercicios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgregarEjercicios));
            this.btmNuevoEjercicio = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIdEjercicio = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtRespuesta = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblErrorPlanteamiento = new System.Windows.Forms.Label();
            this.lblErrorLeccion = new System.Windows.Forms.Label();
            this.lblRegistroEjercicio = new System.Windows.Forms.Label();
            this.lblErrorRespuesta = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPlanteamiento = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btmNuevoEjercicio
            // 
            this.btmNuevoEjercicio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(54)))), ((int)(((byte)(167)))));
            this.btmNuevoEjercicio.FlatAppearance.BorderSize = 0;
            this.btmNuevoEjercicio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmNuevoEjercicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmNuevoEjercicio.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmNuevoEjercicio.ForeColor = System.Drawing.Color.White;
            this.btmNuevoEjercicio.Image = ((System.Drawing.Image)(resources.GetObject("btmNuevoEjercicio.Image")));
            this.btmNuevoEjercicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmNuevoEjercicio.Location = new System.Drawing.Point(236, 368);
            this.btmNuevoEjercicio.Name = "btmNuevoEjercicio";
            this.btmNuevoEjercicio.Size = new System.Drawing.Size(140, 40);
            this.btmNuevoEjercicio.TabIndex = 6;
            this.btmNuevoEjercicio.Text = "Guardar";
            this.btmNuevoEjercicio.UseVisualStyleBackColor = false;
            this.btmNuevoEjercicio.Click += new System.EventHandler(this.btmNuevoEjercicio_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(54)))), ((int)(((byte)(167)))));
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(573, 25);
            this.panel1.TabIndex = 7;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown_1);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(222, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Registro de Ejercicios";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(553, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // txtIdEjercicio
            // 
            this.txtIdEjercicio.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtIdEjercicio.Enabled = false;
            this.txtIdEjercicio.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtIdEjercicio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdEjercicio.HintForeColor = System.Drawing.Color.Empty;
            this.txtIdEjercicio.HintText = "";
            this.txtIdEjercicio.isPassword = false;
            this.txtIdEjercicio.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdEjercicio.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdEjercicio.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdEjercicio.LineThickness = 3;
            this.txtIdEjercicio.Location = new System.Drawing.Point(463, 32);
            this.txtIdEjercicio.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdEjercicio.Name = "txtIdEjercicio";
            this.txtIdEjercicio.Size = new System.Drawing.Size(97, 25);
            this.txtIdEjercicio.TabIndex = 8;
            this.txtIdEjercicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtRespuesta
            // 
            this.txtRespuesta.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtRespuesta.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtRespuesta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRespuesta.HintForeColor = System.Drawing.Color.Empty;
            this.txtRespuesta.HintText = "Respuesta";
            this.txtRespuesta.isPassword = false;
            this.txtRespuesta.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRespuesta.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRespuesta.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRespuesta.LineThickness = 3;
            this.txtRespuesta.Location = new System.Drawing.Point(36, 279);
            this.txtRespuesta.Margin = new System.Windows.Forms.Padding(4);
            this.txtRespuesta.Name = "txtRespuesta";
            this.txtRespuesta.Size = new System.Drawing.Size(182, 33);
            this.txtRespuesta.TabIndex = 8;
            this.txtRespuesta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Planteamiento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label6.Location = new System.Drawing.Point(440, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(34, 258);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Respuesta:";
            // 
            // lblErrorPlanteamiento
            // 
            this.lblErrorPlanteamiento.AutoSize = true;
            this.lblErrorPlanteamiento.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorPlanteamiento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblErrorPlanteamiento.Location = new System.Drawing.Point(23, 217);
            this.lblErrorPlanteamiento.Name = "lblErrorPlanteamiento";
            this.lblErrorPlanteamiento.Size = new System.Drawing.Size(45, 17);
            this.lblErrorPlanteamiento.TabIndex = 10;
            this.lblErrorPlanteamiento.Text = "label6";
            this.lblErrorPlanteamiento.Visible = false;
            // 
            // lblErrorLeccion
            // 
            this.lblErrorLeccion.AutoSize = true;
            this.lblErrorLeccion.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorLeccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblErrorLeccion.Location = new System.Drawing.Point(233, 332);
            this.lblErrorLeccion.Name = "lblErrorLeccion";
            this.lblErrorLeccion.Size = new System.Drawing.Size(45, 17);
            this.lblErrorLeccion.TabIndex = 10;
            this.lblErrorLeccion.Text = "label6";
            this.lblErrorLeccion.Visible = false;
            // 
            // lblRegistroEjercicio
            // 
            this.lblRegistroEjercicio.AutoSize = true;
            this.lblRegistroEjercicio.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistroEjercicio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblRegistroEjercicio.Location = new System.Drawing.Point(193, 332);
            this.lblRegistroEjercicio.Name = "lblRegistroEjercicio";
            this.lblRegistroEjercicio.Size = new System.Drawing.Size(45, 17);
            this.lblRegistroEjercicio.TabIndex = 10;
            this.lblRegistroEjercicio.Text = "label6";
            this.lblRegistroEjercicio.Visible = false;
            // 
            // lblErrorRespuesta
            // 
            this.lblErrorRespuesta.AutoSize = true;
            this.lblErrorRespuesta.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorRespuesta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblErrorRespuesta.Location = new System.Drawing.Point(34, 316);
            this.lblErrorRespuesta.Name = "lblErrorRespuesta";
            this.lblErrorRespuesta.Size = new System.Drawing.Size(45, 17);
            this.lblErrorRespuesta.TabIndex = 10;
            this.lblErrorRespuesta.Text = "label6";
            this.lblErrorRespuesta.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(427, 258);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Lección:";
            // 
            // txtPlanteamiento
            // 
            this.txtPlanteamiento.Location = new System.Drawing.Point(15, 103);
            this.txtPlanteamiento.MaxLength = 550;
            this.txtPlanteamiento.Multiline = true;
            this.txtPlanteamiento.Name = "txtPlanteamiento";
            this.txtPlanteamiento.Size = new System.Drawing.Size(545, 111);
            this.txtPlanteamiento.TabIndex = 14;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(311, 291);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(249, 21);
            this.comboBox1.TabIndex = 15;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.txtPlanteamiento);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblErrorRespuesta);
            this.panel2.Controls.Add(this.lblRegistroEjercicio);
            this.panel2.Controls.Add(this.lblErrorLeccion);
            this.panel2.Controls.Add(this.lblErrorPlanteamiento);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtRespuesta);
            this.panel2.Controls.Add(this.txtIdEjercicio);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btmNuevoEjercicio);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(574, 421);
            this.panel2.TabIndex = 8;
            // 
            // AgregarEjercicios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(574, 421);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AgregarEjercicios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btmNuevoEjercicio;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtIdEjercicio;
        public Bunifu.Framework.UI.BunifuMaterialTextbox txtRespuesta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblErrorPlanteamiento;
        private System.Windows.Forms.Label lblErrorLeccion;
        private System.Windows.Forms.Label lblRegistroEjercicio;
        private System.Windows.Forms.Label lblErrorRespuesta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPlanteamiento;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.ComboBox comboBox1;
    }
}