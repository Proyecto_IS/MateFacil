﻿using CapaDatos;
using CapaPresentacion.InterfacesProfesor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MateFacil
{
    public partial class Avance : Form
    {
       
        CDConexion conexion = new CDConexion();
        SqlCommand cmd = new SqlCommand();

        public Avance()
        {
            InitializeComponent();
        }

       private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bunifuCustomDataGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btmAvanceGrupal_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            chart1.Visible = true;

            cmd = new SqlCommand("select count(distinct Id_Leccion) from Avance", conexion.AbrirConexion());
            Int32 lecciones = Convert.ToInt32(cmd.ExecuteScalar());

            int[] cantAl = new int[lecciones];
            try
            {
                for (int i = 0; i < cantAl.Length; i++)
                {
                    cmd = new SqlCommand("select count (*) from Avance where Id_Leccion like '%" + i + "'", conexion.AbrirConexion());
                    Int32 c = Convert.ToInt32(cmd.ExecuteScalar());
                    cantAl[i] = c;

                }
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //creo que este objeto no hace falta. la grafica en realidad se encuentra en la clase Avance, no en la clase AvanceGrupo
            AvanceGrupo grafica = new AvanceGrupo();

            //este arreglo contiene las lecciones. Van a la derecha de la gráfica
            String[] leccion = new string[cantAl.Length];
            for (int i = 0; i < leccion.Length; i++)
            {
                leccion[i] = "Lección " + i; 
            }

            chart1.Palette = ChartColorPalette.Pastel;
            
            //titulo de la gráfica
            chart1.Titles.Add("Avance Grupal");

            for (int i = 0; i < cantAl.Length; i++)
            {
                Series serie = chart1.Series.Add(leccion[i]);
                serie.Label = cantAl[i].ToString();
                serie.Points.Add(cantAl[i]);
            }
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
