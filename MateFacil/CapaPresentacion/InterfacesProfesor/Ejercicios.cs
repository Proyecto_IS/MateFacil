﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;
using CapaNegocio;
using System.Data.SqlClient;
using MateFacil;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class Ejercicios : Form
    {
        CDConexion conexion = new CDConexion();
        CargarDatosSQL cargar = new CargarDatosSQL();
        public String cadena = Program.curso;


        public Ejercicios()
        {
            InitializeComponent();
            AgregarOpcionesLeccion();
        }
        DataSet Resultado = new DataSet();

        public void AgregarOpcionesLeccion()
        {
            SqlDataAdapter da = new SqlDataAdapter("DeslplegarLecciones ", conexion.AbrirConexion());
            DataTable datos = new DataTable();
            DataSet dt = new DataSet();
            da.Fill(datos);
            if (datos.Rows.Count > 0)
            {
                comboBox1.DisplayMember = "Nomb_Leccion";
                comboBox1.DataSource = datos;
            }
            conexion.CerrarConexion();

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btmModificar_Click(object sender, EventArgs e)
        {

        }

        private void btmEliminar_Click(object sender, EventArgs e)
        {
            String Planteamiento = dgvEjercicios.CurrentRow.Cells["Planteamiento"].Value.ToString();
            String Id = dgvEjercicios.CurrentRow.Cells["Id_Ejercicios"].Value.ToString();

            CDEjercicios objEliminar = new CDEjercicios();
            
            try
            {
                if (dgvEjercicios.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Desea eliminar el ejercicio con ID: " + Id, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        //var aux = new MetodoBusquedaEjercicios();
                        //aux.Listar(dgvEjercicios);
                       
                        objEliminar.EliminarEjercicio(Id);
                        MessageBox.Show("Ejercicio con código "+ Id + " ha sido eliminado");
                        MostrarEjercicios();
                    }
                }
                else
                {
                    MessageBox.Show("Seleccione la fila a eliminar");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error excepción " + ex);
            }
        }

        private void btmNuevo_Click(object sender, EventArgs e)
        {
            AgregarEjercicios objAgregar = new AgregarEjercicios();
            if(objAgregar.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                MostrarEjercicios();
            }
        }

        private void dgvAlumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void Ejercicios_Load(object sender, EventArgs e)
        {
           

        }

        private void Ejercicios_Load_1(object sender, EventArgs e)
        {
             cargar.CargarEjercicios(dgvEjercicios, comboBox1.Text,cadena);
             MostrarEjercicios();
        }
        public void MostrarEjercicios()
        {
            CNEjercicios objeto = new CNEjercicios();
            dgvEjercicios.DataSource = objeto.MostrarEjercicios(comboBox1.Text,cadena);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cargar.CargarEjercicios(dgvEjercicios, comboBox1.Text,cadena);
        }
    }
}
