﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class AvanceGrupal : Form
    {
        public AvanceGrupal()
        {
            InitializeComponent();
        }

        //este método es para mostrar y llenar la gráfica del avance grupal
        private void chart1_Click(object sender, EventArgs e)
        {   
            //el primer arreglo contiene los datos de x y el segundo los datos de y
            string[] series = {"eduardo", "jorge", "gris" };
            int[] puntos = { 23, 10, 15};

            //cambiar los colores
            chart1.Palette = ChartColorPalette.Pastel;

            chart1.Titles.Add("Avance grupal");

            for (int i = 0; i < series.Length; i++)
            {
                //titulos
                Series serie = chart1.Series.Add(series [i]);

                //cantidades
                serie.Label = puntos[i].ToString();

                serie.Points.Add(puntos[i]);
            }
        }
    }
}
