﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using System.Data.SqlClient;
using CapaDatos;
using CapaPresentacion;
using MateFacil;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class AgregarEjercicios : Form
    {
        
        CDConexion conexion = new CDConexion();
        public String cadena = Program.curso;

        int aux = 0;
        Boolean bandera = false;

        public bool Edita = false;
        public AgregarEjercicios()
        {
            InitializeComponent();
            GenerarCodigo();
            AgregarOpcionesLeccion();
            
        }


        public void AgregarOpcionesLeccion()
        {
            SqlDataAdapter da = new SqlDataAdapter("DesplegarLecciones2 '" + cadena + "'", conexion.AbrirConexion());
            DataTable datos = new DataTable();
            DataSet dt = new DataSet();
            da.Fill(datos);
            if (datos.Rows.Count > 0)
            {
                comboBox1.DisplayMember = "Nomb_Leccion";
                comboBox1.DataSource = datos;
            }
        }

        public void GenerarCodigo()
        {
            String codigo = "";
            SqlCommand comando = new SqlCommand("SELECT MAX(Id_Ejercicios) FROM Ejercicios", conexion.AbrirConexion());
            codigo = Convert.ToString(comando.ExecuteScalar());

            if (codigo.Equals(""))
            {
                codigo = "E0001";
            }
            else
            {
                String nuevo = "";
               
                for (int i = 1; i < 5;  i++)
                {
                    nuevo += codigo[i];
                }

                int num = Convert.ToInt32(nuevo);
                num++;
                nuevo = Convert.ToString(num);
                if (nuevo.Length == 1)
                {
                    codigo = "E000" + nuevo;
                }
                else if (nuevo.Length == 2)
                {
                    codigo = "E00" + nuevo;
                }
                else if (nuevo.Length == 3)
                {
                    codigo = "E0" + nuevo;
                }
                else if (nuevo.Length == 4)
                {
                    codigo = "E" + nuevo;
                }


            }
            txtIdEjercicio.Text = codigo;

        }

        private void X_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
        }
        Point lastPoint;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void btmNuevoEjercicio_Click(object sender, EventArgs e)
        {
            CNEjercicios objEjercicio = new CNEjercicios();
            SqlDataReader ConfirmarLeccion;
            CDEjercicios sqlrRegistrarEjercicio = new CDEjercicios();
           
            Ejercicios objDato = new Ejercicios();
            AgregarEjercicios objCerrar = new AgregarEjercicios();

            objEjercicio.IdEjercicios = txtIdEjercicio.Text;
            objEjercicio.Planteamiento = txtPlanteamiento.Text;
            objEjercicio.LeccionEjercicio = comboBox1.Text;
            objEjercicio.Respuesta = txtRespuesta.Text;

            if (Edita == false)
            {
                try
                {
                    if (objEjercicio.IdEjercicios == txtIdEjercicio.Text)
                    {
                        if (objEjercicio.Planteamiento == txtPlanteamiento.Text)
                        {
                            lblErrorPlanteamiento.Visible = false;

                            if (objEjercicio.Respuesta == txtRespuesta.Text)
                            {
                                lblErrorRespuesta.Visible = false;

                                ConfirmarLeccion = objEjercicio.VerficacionDeLeccion();
                                    
                                if (sqlrRegistrarEjercicio.RegistrarEjercicio(txtIdEjercicio.Text, txtPlanteamiento.Text, txtRespuesta.Text, comboBox1.Text,cadena))
                                {
                                    DialogResult = System.Windows.Forms.DialogResult.OK;
                                    objDato.MostrarEjercicios();
                                    
                                    lblRegistroEjercicio.Text = "Ejercicio registrado correctamente";
                                    lblErrorLeccion.Visible = false;
                                    lblRegistroEjercicio.Visible = true;
                                    txtPlanteamiento.Text = "";
                                    txtRespuesta.Text = "";
                                    // txtLeccionEjercicio.Text = "";
                                    GenerarCodigo();
                                                                                                                            
                                }
                                else
                                {
                                    lblErrorLeccion.Text = "Error de almacenamiento de datos";
                                    lblErrorLeccion.Visible = true;
                                }
                                   
                            }
                            else
                            {
                                lblErrorRespuesta.Text = objEjercicio.Respuesta;
                                lblErrorRespuesta.Visible = true;
                            }
                            
                        }
                        else
                        {
                            lblErrorPlanteamiento.Text = objEjercicio.Planteamiento;
                            lblErrorPlanteamiento.Visible = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error ID Ejercicio");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error excepción: " + ex);
                }
            }

           
        }


        

        private void lblErrorGrupoAlumno_Click(object sender, EventArgs e)
        {

        }

        private void txtGrupoAlumno_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void lblErrorNombreAlumno_Click(object sender, EventArgs e)
        {

        }
        private void txtLeccionEjercicio_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.SoloNumeros(e);
        }

        private void txtPlanteamiento_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void AgregarEjercicios_Load(object sender, EventArgs e)
        {
            AgregarOpcionesLeccion();

        }

        private void txtLeccionEjercicio_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void txtPlanteamiento2_KeyPress(object sender, KeyPressEventArgs e)
        {
            int linea = txtPlanteamiento.GetLineFromCharIndex(txtPlanteamiento.SelectionStart);
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                Convert.ToChar(Keys.Enter);
                bandera = true;
                aux = linea + 1;
            }

           
        }


        

        private void txtPlanteamiento_KeyUp(object sender, KeyEventArgs e)
        {
            int linea = txtPlanteamiento.GetLineFromCharIndex(txtPlanteamiento.SelectionStart);

            if (bandera == false)
            {
                if (aux < linea)
                {
                    Char enter = Convert.ToChar(Keys.Enter);
                    SendKeys.Send(" " + enter);
                    aux = linea;

                }
            }
            bandera = false;
        }

        private void panel1_MouseDown_1(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
    }
}
