﻿using System;
using System.Drawing;
using CapaNegocio;
using System.Windows.Forms;
using System.Data.SqlClient;
using CapaDatos;
using System.Data;
using System.Text.RegularExpressions;
namespace MateFacil
{
    public partial class AgregarAlumno : Form
    {
        CDConexion conexion = new CDConexion();
        CargarDatosSQL cargar = new CargarDatosSQL();
        Alumnos objDato = new Alumnos();
        string idcal = "";

        public String cadena = Program.curso;

        public bool Edita = false;
        public bool SobreCarga = false;
        public AgregarAlumno()
        {
            InitializeComponent();
            GenerarCodigoAlumno();
            AgregarOpcionesGrupo();
        }
        public bool EsValido()
        {
            return !string.IsNullOrWhiteSpace(this.Text);
        }
        public void AgregarOpcionesGrupo()
        {
            SqlDataAdapter da = new SqlDataAdapter("Select * from Datos_Grupo", conexion.AbrirConexion());
            DataTable datos = new DataTable();
            DataSet dt = new DataSet();
            da.Fill(datos);
            if (datos.Rows.Count > 0)
            {
                comboBox1.DisplayMember = "Nombre_Grupo";
                comboBox1.DataSource = datos;
            }
        }

        public void GenerarCodigoCalificación()
        {
            
            SqlCommand comando = new SqlCommand("SELECT MAX(Id_Calificacion) FROM Calificaciones", conexion.AbrirConexion());
            idcal = Convert.ToString(comando.ExecuteScalar());

            if (idcal.Equals(""))
            {
                idcal = "V0001";
            }
            else
            {
                string nuevo = "";

                for (int i = 1; i < 5; i++)
                {
                    nuevo += idcal[i];
                }

                int num = Convert.ToInt32(nuevo);
                num++;
                nuevo = Convert.ToString(num);
                if (nuevo.Length == 1)
                {
                    idcal = "V000" + nuevo;
                }
                else if (nuevo.Length == 2)
                {
                    idcal = "V00" + nuevo;
                }
                else if (nuevo.Length == 3)
                {
                    idcal = "V0" + nuevo;
                }
                else if (nuevo.Length == 4)
                {
                    idcal = "V" + nuevo;
                }


            }


        }
        public void GenerarCodigoAlumno()
        {
            String codigo = "";
            SqlCommand comando = new SqlCommand("SELECT MAX(Id_Alumno) FROM Alumno", conexion.AbrirConexion());
            codigo = Convert.ToString(comando.ExecuteScalar());

            if (codigo.Equals(""))
            {
                codigo = "A0001";
            }
            else
            {
                String nuevo = "";

                for (int i = 1; i < 5; i++)
                {
                    nuevo += codigo[i];
                }

                int num = Convert.ToInt32(nuevo);
                num++;
                nuevo = Convert.ToString(num);
                if (nuevo.Length==1)
                {
                    codigo = "A000" + nuevo;
                }
                else if(nuevo.Length==2)
                {
                    codigo = "A00" + nuevo;
                }
                else if (nuevo.Length == 3)
                {
                    codigo = "A0" + nuevo;
                }
                else if (nuevo.Length == 4)
                {
                    codigo = "A" + nuevo;
                }


            }
            txtIdAlumno.Text = codigo;

        }
        private void X_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            cargar.CargarAlumnos(objDato.dgvAlumnos,cadena);
            cargar.CargarGrupos(objDato.dgvGrupos);
            this.Close();
            
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        Point lastPoint;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void btmNuevoAlumno_Click(object sender, EventArgs e)
        {
            CNAlumno objAlumno = new CNAlumno();
            CDAlumno sqlrRegistrarAlumno = new CDAlumno();
            AgregarAlumno objCerrar = new AgregarAlumno();
            Validaciones validar = new Validaciones();
         

            SqlDataReader ConfirAlumno;

            objAlumno.IdAlumno = txtIdAlumno.Text;
            objAlumno.NombreAlumno = txtNombreAlumno.Text;
            objAlumno.GrupoAlumno = comboBox1.Text;
            objAlumno.GradoAlumno = comboBox2.Text;

            GenerarCodigoCalificación();

            if (Edita == false)
            {
                try
                {
                    if (objAlumno.IdAlumno == txtIdAlumno.Text)
                    {
                        if (objAlumno.NombreAlumno == txtNombreAlumno.Text)
                        {
                            lblErrorNombreAlumno.Visible = false;

                        if (objAlumno.GradoAlumno == comboBox2.Text)
                            {
                                lblErrorGradoAlumno.Visible = false;

                                ConfirAlumno = objAlumno.VerificacionDelAlumno();

                                if (ConfirAlumno.Read() == true)
                                {
                                    lblErrorNombreAlumno.Text = "El nombre ingresado ya existe";
                                    lblErrorNombreAlumno.ForeColor = Color.Red;
                                    lblErrorNombreAlumno.Visible = true;
                                }
                                else
                                {
                                    if (sqlrRegistrarAlumno.RegistrarAlumno(txtIdAlumno.Text, txtNombreAlumno.Text, comboBox2.Text, comboBox1.Text, cadena,idcal))
                                    {

                                        lblRegistroAlumno.Text = "Alumno Registrado Correctamente";
                                        lblErrorGrupo.Visible = false;
                                        lblRegistroAlumno.Visible = true;
                                        txtNombreAlumno.Text = " ";
                                        idcal = "";
                                        GenerarCodigoAlumno();
                                        SobreCarga = true;
                                        DialogResult = DialogResult.OK;
                                    }
                                    else
                                    {
                                        lblErrorGrupo.Text = "Error de almacenamiento de datos";
                                        lblErrorGrupo.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                lblErrorGradoAlumno.Text = objAlumno.GradoAlumno;
                                lblErrorGradoAlumno.Visible = true;
                            }
                        }

                        else                      
                        {
                                lblErrorNombreAlumno.ForeColor = Color.Green;
                                lblErrorNombreAlumno.Text = objAlumno.NombreAlumno;
                                lblErrorNombreAlumno.Visible = true;

                        }

                    }

                    else

                    {
                            MessageBox.Show("Error ID Alumno");

                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error excepción: " + ex);
                }
            }
            
            if(Edita == true)
            {
                try
                {
                    if (objAlumno.IdAlumno == txtIdAlumno.Text)
                    {
                        if (objAlumno.NombreAlumno == txtNombreAlumno.Text)
                        {
                            lblErrorNombreAlumno.Visible = false;                       

                            if (objAlumno.GradoAlumno == comboBox2.Text)
                            {
                                lblErrorGradoAlumno.Visible = false;

                                if (MessageBox.Show("Desea modificarlo ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    if (sqlrRegistrarAlumno.ModificarAlumno(txtIdAlumno.Text, txtNombreAlumno.Text, comboBox2.Text, comboBox1.Text))
                                    {  
                                        lblRegistroAlumno.Text = "Alumno modificado correctamente";
                                        lblErrorGrupo.Visible = false;
                                        lblRegistroAlumno.Visible = true;
                                        Edita = false;
                                        DialogResult = System.Windows.Forms.DialogResult.OK;
                                    }
                                    else
                                    {
                                        lblErrorGrupo.Text = "Error de almacenamiento de datos";
                                        lblErrorGrupo.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                lblErrorGradoAlumno.Text = objAlumno.GradoAlumno;
                                lblErrorGradoAlumno.Visible = true;
                            }
                        }
                        else
                        {
                                
                        }
                       
                    }
                }catch(Exception ex)
                {
                    MessageBox.Show("Error Excepción " +ex);
                }
            }
        }

        private void txtIdAlumno_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void btmModificar_Click(object sender, EventArgs e)
        {

        }

        private void txtNombreAlumno_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.ValidaAgregarAlumno(e);
        }

        private void txtGradoAlumno_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.ValidaGrupoAlumno(e);
        }

        private void txtGrupoAlumno_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.SoloNumeros(e);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtGrupoAlumno_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
