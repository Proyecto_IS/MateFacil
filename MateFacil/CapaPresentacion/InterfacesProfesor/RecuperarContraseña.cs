﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

using CapaNegocio;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class RecuperarContraseña : Form
    {
        CNUsuario objCN = new CNUsuario();
        public RecuperarContraseña()
        {
            InitializeComponent();
        }

        private void btmRecuperar_Click(object sender, EventArgs e)
        {
            CNUsuario objCN = new CNUsuario();
            objCN.Dni = txtUsuario.Text;

            SqlDataReader confirmarUsuario;
            
            if(objCN.Dni == txtUsuario.Text)
            {
                lblErrorDni.Visible = false;

                confirmarUsuario = objCN.ConfirmarUsuarioPass();
                if (confirmarUsuario.Read() == true)
                {
                    txtMensaje.Text = objCN.RecuPass(txtUsuario.Text);
                }
                else
                {
                    lblErrorDni.Text = "El nombre de usuario no existe";
                    lblErrorDni.ForeColor = Color.Red;
                    lblErrorDni.Visible = true;
                }
            }
            else
            {
                lblErrorDni.ForeColor = Color.Green;
                lblErrorDni.Text = objCN.Dni;
                lblErrorDni.Visible = true;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        Point lastPoint;

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones.ValidarNombreUsuario(e);
        }
    }
}
