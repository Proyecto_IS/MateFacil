﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;
using System.Data.SqlClient;
using CapaPresentacion;
using CapaDatos;
using MateFacil;
using CapaPresentacion.InterfacesAlumno;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class Lecciones : Form
    {
        CDConexion conexion = new CDConexion();
        CargarDatosSQL edita = new CargarDatosSQL();
        public String codigo = "";

        public String cadena = Program.curso;

       
        public Lecciones()
        {
           InitializeComponent();
        }

        public void GenerarCodigo()
        {
          
            SqlCommand comando = new SqlCommand("SELECT MAX(Id_Contenido) FROM Contenido", conexion.AbrirConexion());
            codigo = Convert.ToString(comando.ExecuteScalar());

            if (codigo.Equals(""))
            {
                codigo = "T00001";
            }
            else
            {
                String nuevo = "";

                for (int i = 1; i < 6; i++)
                {
                    nuevo += codigo[i];
                }

                int num = Convert.ToInt32(nuevo);
                num++;
                nuevo = Convert.ToString(num);
                if (nuevo.Length == 1)
                {
                    codigo = "T0000" + nuevo;
                }
                else if (nuevo.Length == 2)
                {
                    codigo = "T000" + nuevo;
                }
                else if (nuevo.Length == 3)
                {
                    codigo = "T00" + nuevo;
                }
                else if (nuevo.Length == 4)
                {
                    codigo = "T0" + nuevo;
                }
                else if (nuevo.Length == 5)
                {
                    codigo = "T" + nuevo;
                }


            }
            conexion.CerrarConexion();

        }

        public void AgregarOpcionesLeccion2()
        {
            SqlDataAdapter da2 = new SqlDataAdapter("Select * from Lecciones", conexion.AbrirConexion());
            DataTable datos2 = new DataTable();
            DataSet dt2 = new DataSet();
            da2.Fill(datos2);
            if (datos2.Rows.Count > 0)
            {
                //LeccionesBD.DataSource = datos2;
                //LeccionesBD.DisplayMember = "Nomb_Leccion";
                LBD.DataSource = datos2;
                LBD.DisplayMember = "Nomb_Leccion";

            }
        }

        public void AgregarOpcionesLeccion()
        {
            
            SqlDataAdapter da = new SqlDataAdapter("DesplegarLecciones2 '"+cadena+"'", conexion.AbrirConexion());
            DataTable datos = new DataTable();
            DataSet dt = new DataSet();
            da.Fill(datos);
            if (datos.Rows.Count >= 0)
            {
                //ListaLecciones.DataSource = datos;
                //ListaLecciones.DisplayMember = "Nomb_Leccion";
                LeccionesCurso.DataSource = datos;
                LeccionesCurso.DisplayMember = "Nomb_Leccion";

            }

            


        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btmAgregarLeccion_Click(object sender, EventArgs e)
        {

            String leccion = LBD.Text;
          
            GenerarCodigo();


            SqlCommand comando2 = new SqlCommand("BuscarLecciones '"+leccion+"','"+cadena+"'",conexion.AbrirConexion());
            SqlDataReader leer = comando2.ExecuteReader();


            if (leer.Read() == true)
            {
                MessageBox.Show("La lección existe en el curso.");

                conexion.CerrarConexion();


            }
            else
            {
                

                conexion.CerrarConexion();
                SqlDataAdapter da = new SqlDataAdapter("InsertarLeccion'" + codigo + "','" + leccion + "','" + cadena + "'", conexion.AbrirConexion());
                DataTable datos = new DataTable();
                DataSet dt = new DataSet();
                da.Fill(datos);
                if (datos.Rows.Count > 0)
                {
                    LeccionesCurso.DataSource = datos;
                    LeccionesCurso.DisplayMember = "Nomb_Leccion";

                }
                AgregarOpcionesLeccion();
                conexion.CerrarConexion();

            }
        }

        private void btmEliminarLeccion_Click(object sender, EventArgs e)
        {
            String leccion = LeccionesCurso.Text;

            try
            {
                if (LeccionesCurso.SelectedItems.Count >0)
                {
                    if (MessageBox.Show("¿ Desea eliminar la lección " + leccion, " del curso ? Si elimina la presente leccíon los alumnos ya no podrán acceder a ella pero se conserván sus resultados.", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        SqlDataAdapter da = new SqlDataAdapter("EliminarLeccion'"+ leccion+ "','"+cadena+"'", conexion.AbrirConexion());
                        DataTable datos = new DataTable();
                        DataSet dt = new DataSet();
                        da.Fill(datos);
                        if (datos.Rows.Count > 0)
                        {
                            LeccionesCurso.DataSource = datos;
                            LeccionesCurso.DisplayMember = "Nomb_Leccion";

                        }
                        AgregarOpcionesLeccion();
                        conexion.CerrarConexion();
                        MessageBox.Show("La lección "+leccion + " ha sido eliminada del Curso.");

                    }
                }
                else
                {
                    MessageBox.Show("Seleccione la leccón a eliminar.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error excepción " + ex);
            }

           
            
        }

        private void Lecciones_Load(object sender, EventArgs e)
        {
            AgregarOpcionesLeccion();
            AgregarOpcionesLeccion2();
        }

        private void dgvLecciones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Buscar_TextChanged(object sender, EventArgs e)
        {

        }

        private void CheckLecciones_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LeccionesBD_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Copia1 l1 = new Copia1();
            Copia2 l2 = new Copia2();
            Copia3 l3 = new Copia3();
            String leccion = LBD.Text;
            if (leccion.Equals("Convertir Decimales-Fracciones"))
            {
                if (l1.ShowDialog() == DialogResult.OK)
                {
                    l1.Show();
                }
            }
            if (leccion.Equals("Convertir Fracciones-Decimales"))
            {
                if (l2.ShowDialog() == DialogResult.OK)
                {
                    l2.Show();
                }
            }
            if (leccion.Equals("Suma y resta de fracciones"))
            {
                if (l3.ShowDialog() == DialogResult.OK)
                {
                    l3.Show();
                }
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            diag8 d = new diag8();
            if (d.ShowDialog() == DialogResult.OK)
            {
                d.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Test1 l2 = new Test1();
            Test2 l1 = new Test2();
            Test3 l3 = new Test3();
            String leccion = LBD.Text;
            if (leccion.Equals("Convertir Decimales-Fracciones"))
            {
                if (l2.ShowDialog() == DialogResult.OK)
                {
                    l2.Show();
                }
            }
            if (leccion.Equals("Convertir Fracciones-Decimales"))
            {
                if (l1.ShowDialog() == DialogResult.OK)
                {
                    l1.Show();
                }
            }
            if (leccion.Equals("Suma y resta de fracciones"))
            {
                if (l3.ShowDialog() == DialogResult.OK)
                {
                    l3.Show();
                }
            }

        }
    }
}
