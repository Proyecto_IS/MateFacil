﻿namespace MateFacil
{
    partial class Avance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Avance));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btmAvanceIndividual = new System.Windows.Forms.Button();
            this.btmAvanceGrupal = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // btmAvanceIndividual
            // 
            this.btmAvanceIndividual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmAvanceIndividual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(31)))));
            this.btmAvanceIndividual.FlatAppearance.BorderSize = 0;
            this.btmAvanceIndividual.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmAvanceIndividual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmAvanceIndividual.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvanceIndividual.ForeColor = System.Drawing.Color.White;
            this.btmAvanceIndividual.Image = ((System.Drawing.Image)(resources.GetObject("btmAvanceIndividual.Image")));
            this.btmAvanceIndividual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmAvanceIndividual.Location = new System.Drawing.Point(64, 469);
            this.btmAvanceIndividual.Name = "btmAvanceIndividual";
            this.btmAvanceIndividual.Size = new System.Drawing.Size(212, 40);
            this.btmAvanceIndividual.TabIndex = 8;
            this.btmAvanceIndividual.Text = "Avance Individual";
            this.btmAvanceIndividual.UseVisualStyleBackColor = false;
            // 
            // btmAvanceGrupal
            // 
            this.btmAvanceGrupal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmAvanceGrupal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(31)))));
            this.btmAvanceGrupal.FlatAppearance.BorderSize = 0;
            this.btmAvanceGrupal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmAvanceGrupal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmAvanceGrupal.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAvanceGrupal.ForeColor = System.Drawing.Color.White;
            this.btmAvanceGrupal.Image = ((System.Drawing.Image)(resources.GetObject("btmAvanceGrupal.Image")));
            this.btmAvanceGrupal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmAvanceGrupal.Location = new System.Drawing.Point(659, 469);
            this.btmAvanceGrupal.Name = "btmAvanceGrupal";
            this.btmAvanceGrupal.Size = new System.Drawing.Size(212, 40);
            this.btmAvanceGrupal.TabIndex = 9;
            this.btmAvanceGrupal.Text = "Avance Grupal";
            this.btmAvanceGrupal.UseVisualStyleBackColor = false;
            this.btmAvanceGrupal.Click += new System.EventHandler(this.btmAvanceGrupal_Click);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(182, 41);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(573, 306);
            this.chart1.TabIndex = 10;
            this.chart1.Text = "chart1";
            this.chart1.Visible = false;
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // Avance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(967, 602);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.btmAvanceGrupal);
            this.Controls.Add(this.btmAvanceIndividual);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Avance";
            this.Text = "AvanceIndividual";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btmAvanceIndividual;
        private System.Windows.Forms.Button btmAvanceGrupal;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}