﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using CapaDatos;
using CapaNegocio;

namespace MateFacil
{
    public partial class Alumnos : Form
    {
        CDConexion conexion = new CDConexion();
        CDAlumno objetoCN = new CDAlumno();
        CargarDatosSQL cargar = new CargarDatosSQL();
        MetodoBusqueda aux = new MetodoBusqueda();

        public String cadena = Program.curso;

        public Alumnos()
        {
            InitializeComponent();
            
        }
        DataSet Resultado = new DataSet();

       
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            AgregarAlumno agregar = new AgregarAlumno();
            if(agregar.ShowDialog() == DialogResult.OK)
            {
                MostrarAlumnos();
            }
        }

        private void dtvAlumnos_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void Recargar()
        {
            aux.Listar(dgvAlumnos,cadena);
            //cargar.CargarGrupos(dgvGrupos);
            //cargar.CargarAlumnos(dgvAlumnos);
        }

        private void Alumnos_Load(object sender, EventArgs e)
        {
            var aux = new MetodoBusqueda();
            aux.Listar(dgvAlumnos,cadena);
            //MostrarAlumnos();
           // cargar.CargarGrupos(dgvGrupos);
            cargar.CargarAlumnos(dgvAlumnos,cadena);

        }
        public void MostrarAlumnos()
        {
            CNAlumno objeto = new CNAlumno();
            dgvAlumnos.DataSource = objeto.MostraraAlumno(cadena);
        }
        private void btmEliminar_Click(object sender, EventArgs e)
        {
            String Nombre = dgvAlumnos.CurrentRow.Cells["Nombre_Alumno"].Value.ToString();
            String Id = dgvAlumnos.CurrentRow.Cells["Id_Alumno"].Value.ToString();
            
            CDAlumno objEliminar = new CDAlumno();
            try
            {
                if (dgvAlumnos.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("¡¡¡ADVERTENCIA!!!. Si elimina al alumno eliminará su avance,calificaciones e inicio de sesión. ¿Seguro que desea eliminar al alumno ?"+Nombre,"", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var aux = new MetodoBusqueda();
                        aux.Listar(dgvAlumnos,cadena);
                        objEliminar.EliminarAlunmo(Nombre,Id);
                        aux.Listar(dgvAlumnos, cadena);
                        //MostrarAlumnos();
                        MessageBox.Show(Nombre + " ha sido eliminado");
                        
                    }
                }
                else
                {
                    MessageBox.Show("Seleccione la fila a Eliminar");
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Error excepción " + ex);
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            var aux = new MetodoBusqueda();
            aux.Filtrar(dgvAlumnos, this.txtBuscar.Text.Trim(),cadena);
        }

        private void dgvAlumnos_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void btmModificar_Click(object sender, EventArgs e)
        {
            AgregarAlumno objAgregar = new AgregarAlumno();

            try
            {
                if (dgvAlumnos.SelectedRows.Count > 0)
                {
                    objAgregar.txtIdAlumno.Text = dgvAlumnos.CurrentRow.Cells["Id_Alumno"].Value.ToString();
                    objAgregar.txtNombreAlumno.Text = dgvAlumnos.CurrentRow.Cells["Nombre_Alumno"].Value.ToString();
                    objAgregar.comboBox2.Text = dgvAlumnos.CurrentRow.Cells["Grado"].Value.ToString();
                   // objAgregar.txtGrupoAlumno.Text = dgvAlumnos.CurrentRow.Cells["Id_Grupo"].Value.ToString();

                    objAgregar.Edita = true;
                    if(objAgregar.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        aux.Listar(dgvAlumnos, cadena);
                        //MostrarAlumnos();
                    }
                   
                }
                else
                {
                    MessageBox.Show("Seleccione una fila por favor");
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Error Excepción " + ex);
            }
        }

        private void dgvAlumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMaterialTextbox1_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dgvGrupos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
