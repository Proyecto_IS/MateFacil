﻿namespace CapaPresentacion.InterfacesProfesor
{
    partial class BaseDeDatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseDeDatos));
            this.txtBackup = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtRestore = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.btmUbicacionCopia = new System.Windows.Forms.Button();
            this.btmCopiaBD = new System.Windows.Forms.Button();
            this.btmUbicacionRes = new System.Windows.Forms.Button();
            this.btmResBD = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtBackup
            // 
            this.txtBackup.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBackup.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtBackup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtBackup.HintForeColor = System.Drawing.Color.Empty;
            this.txtBackup.HintText = "Ingrese la ubicación para la copia de seguridad";
            this.txtBackup.isPassword = false;
            this.txtBackup.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtBackup.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtBackup.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtBackup.LineThickness = 3;
            this.txtBackup.Location = new System.Drawing.Point(150, 153);
            this.txtBackup.Margin = new System.Windows.Forms.Padding(4);
            this.txtBackup.Name = "txtBackup";
            this.txtBackup.Size = new System.Drawing.Size(689, 33);
            this.txtBackup.TabIndex = 9;
            this.txtBackup.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtRestore
            // 
            this.txtRestore.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtRestore.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtRestore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRestore.HintForeColor = System.Drawing.Color.Empty;
            this.txtRestore.HintText = "Ingrese la ubicación donde se llevará acabo la restauración";
            this.txtRestore.isPassword = false;
            this.txtRestore.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRestore.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRestore.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtRestore.LineThickness = 3;
            this.txtRestore.Location = new System.Drawing.Point(150, 353);
            this.txtRestore.Margin = new System.Windows.Forms.Padding(4);
            this.txtRestore.Name = "txtRestore";
            this.txtRestore.Size = new System.Drawing.Size(689, 33);
            this.txtRestore.TabIndex = 9;
            this.txtRestore.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // btmUbicacionCopia
            // 
            this.btmUbicacionCopia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmUbicacionCopia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(31)))));
            this.btmUbicacionCopia.FlatAppearance.BorderSize = 0;
            this.btmUbicacionCopia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmUbicacionCopia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmUbicacionCopia.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmUbicacionCopia.ForeColor = System.Drawing.Color.White;
            this.btmUbicacionCopia.Image = ((System.Drawing.Image)(resources.GetObject("btmUbicacionCopia.Image")));
            this.btmUbicacionCopia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmUbicacionCopia.Location = new System.Drawing.Point(203, 231);
            this.btmUbicacionCopia.Name = "btmUbicacionCopia";
            this.btmUbicacionCopia.Size = new System.Drawing.Size(212, 40);
            this.btmUbicacionCopia.TabIndex = 10;
            this.btmUbicacionCopia.Text = "Ubicación";
            this.btmUbicacionCopia.UseVisualStyleBackColor = false;
            this.btmUbicacionCopia.Click += new System.EventHandler(this.btmUbicacionCopia_Click);
            // 
            // btmCopiaBD
            // 
            this.btmCopiaBD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmCopiaBD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(31)))));
            this.btmCopiaBD.FlatAppearance.BorderSize = 0;
            this.btmCopiaBD.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmCopiaBD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmCopiaBD.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmCopiaBD.ForeColor = System.Drawing.Color.White;
            this.btmCopiaBD.Image = ((System.Drawing.Image)(resources.GetObject("btmCopiaBD.Image")));
            this.btmCopiaBD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmCopiaBD.Location = new System.Drawing.Point(514, 231);
            this.btmCopiaBD.Name = "btmCopiaBD";
            this.btmCopiaBD.Size = new System.Drawing.Size(212, 40);
            this.btmCopiaBD.TabIndex = 10;
            this.btmCopiaBD.Text = "    Hacer copia BD";
            this.btmCopiaBD.UseVisualStyleBackColor = false;
            this.btmCopiaBD.Click += new System.EventHandler(this.btmCopiaBD_Click);
            // 
            // btmUbicacionRes
            // 
            this.btmUbicacionRes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmUbicacionRes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(31)))));
            this.btmUbicacionRes.FlatAppearance.BorderSize = 0;
            this.btmUbicacionRes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmUbicacionRes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmUbicacionRes.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmUbicacionRes.ForeColor = System.Drawing.Color.White;
            this.btmUbicacionRes.Image = ((System.Drawing.Image)(resources.GetObject("btmUbicacionRes.Image")));
            this.btmUbicacionRes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmUbicacionRes.Location = new System.Drawing.Point(203, 449);
            this.btmUbicacionRes.Name = "btmUbicacionRes";
            this.btmUbicacionRes.Size = new System.Drawing.Size(212, 40);
            this.btmUbicacionRes.TabIndex = 10;
            this.btmUbicacionRes.Text = "Ubicación";
            this.btmUbicacionRes.UseVisualStyleBackColor = false;
            this.btmUbicacionRes.Click += new System.EventHandler(this.btmUbicacionRes_Click);
            // 
            // btmResBD
            // 
            this.btmResBD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmResBD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(31)))));
            this.btmResBD.FlatAppearance.BorderSize = 0;
            this.btmResBD.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmResBD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmResBD.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmResBD.ForeColor = System.Drawing.Color.White;
            this.btmResBD.Image = ((System.Drawing.Image)(resources.GetObject("btmResBD.Image")));
            this.btmResBD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmResBD.Location = new System.Drawing.Point(514, 449);
            this.btmResBD.Name = "btmResBD";
            this.btmResBD.Size = new System.Drawing.Size(212, 40);
            this.btmResBD.TabIndex = 10;
            this.btmResBD.Text = "Restaurar BD";
            this.btmResBD.UseVisualStyleBackColor = false;
            this.btmResBD.Click += new System.EventHandler(this.btmResBD_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(277, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(409, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Respaldo de la base de datos";
            // 
            // BaseDeDatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(967, 602);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btmResBD);
            this.Controls.Add(this.btmUbicacionRes);
            this.Controls.Add(this.btmCopiaBD);
            this.Controls.Add(this.btmUbicacionCopia);
            this.Controls.Add(this.txtRestore);
            this.Controls.Add(this.txtBackup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BaseDeDatos";
            this.Text = "BaseDeDatos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Bunifu.Framework.UI.BunifuMaterialTextbox txtBackup;
        public Bunifu.Framework.UI.BunifuMaterialTextbox txtRestore;
        private System.Windows.Forms.Button btmUbicacionCopia;
        private System.Windows.Forms.Button btmCopiaBD;
        private System.Windows.Forms.Button btmUbicacionRes;
        private System.Windows.Forms.Button btmResBD;
        private System.Windows.Forms.Label label1;
    }
}