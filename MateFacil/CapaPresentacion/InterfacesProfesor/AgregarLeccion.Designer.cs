﻿namespace CapaPresentacion.InterfacesProfesor
{
    partial class AgregarLeccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.btnAceptarLeccion = new System.Windows.Forms.Button();
            this.btnCancelarLeccion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxBloqueLeccion = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Lección 01: Sumas",
            "Lección 02: Fracciones",
            "Lección 03: Decimales"});
            this.checkedListBox1.Location = new System.Drawing.Point(46, 100);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(251, 109);
            this.checkedListBox1.TabIndex = 0;
            // 
            // btnAceptarLeccion
            // 
            this.btnAceptarLeccion.Location = new System.Drawing.Point(41, 248);
            this.btnAceptarLeccion.Name = "btnAceptarLeccion";
            this.btnAceptarLeccion.Size = new System.Drawing.Size(75, 23);
            this.btnAceptarLeccion.TabIndex = 1;
            this.btnAceptarLeccion.Text = "Aceptar";
            this.btnAceptarLeccion.UseVisualStyleBackColor = true;
            // 
            // btnCancelarLeccion
            // 
            this.btnCancelarLeccion.Location = new System.Drawing.Point(217, 248);
            this.btnCancelarLeccion.Name = "btnCancelarLeccion";
            this.btnCancelarLeccion.Size = new System.Drawing.Size(75, 23);
            this.btnCancelarLeccion.TabIndex = 2;
            this.btnCancelarLeccion.Text = "Cancelar";
            this.btnCancelarLeccion.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Lecciones:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Bloque:";
            // 
            // comboBoxBloqueLeccion
            // 
            this.comboBoxBloqueLeccion.FormattingEnabled = true;
            this.comboBoxBloqueLeccion.Location = new System.Drawing.Point(115, 34);
            this.comboBoxBloqueLeccion.Name = "comboBoxBloqueLeccion";
            this.comboBoxBloqueLeccion.Size = new System.Drawing.Size(121, 21);
            this.comboBoxBloqueLeccion.TabIndex = 5;
            // 
            // AgregarLeccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 296);
            this.Controls.Add(this.comboBoxBloqueLeccion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelarLeccion);
            this.Controls.Add(this.btnAceptarLeccion);
            this.Controls.Add(this.checkedListBox1);
            this.Name = "AgregarLeccion";
            this.Text = "AgregarLeccion";
            this.Load += new System.EventHandler(this.AgregarLeccion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button btnAceptarLeccion;
        private System.Windows.Forms.Button btnCancelarLeccion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxBloqueLeccion;
    }
}