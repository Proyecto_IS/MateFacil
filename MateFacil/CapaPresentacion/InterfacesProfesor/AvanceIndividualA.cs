﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using CapaNegocio;
using CapaDatos;

using Microsoft.Office.Interop.Excel;
using MateFacil;

namespace CapaPresentacion.InterfacesProfesor
{
    public partial class AvanceIndividualA : Form
    {
        MetodoBusquedaEjercicios busqueda = new MetodoBusquedaEjercicios();

        public string cadena = Program.curso;
        public AvanceIndividualA()
        {
            InitializeComponent();
        }

        private void AvanceIndividualA_Load(object sender, EventArgs e)
        {
            busqueda.autoCompletar(txtBuscar,cadena);
        }
        public void MostrarAlumno()
        {
            CNAvance objeto = new CNAvance();
            dgvAlumno.DataSource = objeto.MostrarAlumno(txtBuscar.Text,cadena);
        }

        private void btmBuscar_Click(object sender, EventArgs e)
        {
            CNAvance objBuscar = new CNAvance();
            CDAvances Objeto = new CDAvances();

            objBuscar.BuscarAlumno = txtBuscar.Text;

            SqlDataReader ConfirmarAlumno;

            if (txtBuscar.Text == objBuscar.BuscarAlumno)
            {
                lbErrorAlumno.Visible = false;
                ConfirmarAlumno = objBuscar.ConfirmarcAlumno();
                if (ConfirmarAlumno.Read() == true)
                {
                    lbErrorAlumno.Visible = false;
                    MostrarAlumno();

                }
                else
                {
                    lbErrorAlumno.Text = "El nombre que ingreso no existe";
                    lbErrorAlumno.ForeColor = Color.Red;
                    lbErrorAlumno.Visible = true;
                }
            }
            else
            {
                lbErrorAlumno.ForeColor = Color.Blue;
                lbErrorAlumno.Text = objBuscar.BuscarAlumno;
                lbErrorAlumno.Visible = true;
            }
        }


        private void btmExcel_Click(object sender, EventArgs e)
        {
            Validaciones ob = new Validaciones();
            ob.ExportarDataGridViewExcel(dgvAlumno);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if((e.KeyChar >= 65 && e.KeyChar <=90 )|| (e.KeyChar >= 97 && e.KeyChar <= 122) )
            {
                e.Handled = false;
            }
            else if(e.KeyChar == 13)
            {
                btmBuscar_Click(sender,e);
            }
            else
            {
                e.Handled = true;
            }
        }

       
    }
}
