﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CapaDatos;

namespace MateFacil
{
    public partial class Form1 : Form
    {

        
        public Form1()
        {
            InitializeComponent();
        }

        CDConexion conexion = new CDConexion();

        private void iconcerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void iconmaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            iconrestaurar.Visible = true;
            iconmaximizar.Visible = false;
        }

        private void iconrestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            iconrestaurar.Visible = false;
            iconmaximizar.Visible = true;
        }

        private void iconminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        Point lastPoint;
        private void BarraTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        public void AbrirFormInPanel(object Formhijo)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();
        }
        private void Menu_Click(object sender, EventArgs e)
        {
            
        }

        private void Menu_Click_1(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 77)
            {
                MenuVertical.Visible = false;
                MenuVertical.Width = 285;
                PanelAnimacion2.ShowSync(MenuVertical);
            }
            else
            {
                MenuVertical.Visible = false;
                MenuVertical.Width = 77;
                PanelAnimacion.ShowSync(MenuVertical);
            }
        }

        private void btnAlumnos_Click_1(object sender, EventArgs e)
        {
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            panelAvance.Visible = false;
            AbrirFormInPanel(new Alumnos());
        }

        private void btmAvances_Click_1(object sender, EventArgs e)
        {
            if (panelAvance.Visible == true)
            {
                panelAvance.Visible = false;
            }
            else
            {
                panelAvance.Visible = true;
            }
                    
            btmLecciones.Location = new Point(0, 508);
            btmEjecicios.Location = new Point(0, 566);
            btmBaseDeDatos.Location = new Point(0, 600);
            panelAvance.Location = new Point(42, 396);

            if (panelAvance.Visible == false)
            {
                btmLecciones.Location = new Point(0, 406);
                btmEjecicios.Location = new Point(0, 475);
                btmBaseDeDatos.Location = new Point(0, 539);
            }
        }

        private void btmLecciones_Click_1(object sender, EventArgs e)
        {
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            AbrirFormInPanel(new CapaPresentacion.InterfacesProfesor.Lecciones());
            panelAvance.Visible = false;
        }

        private void btmEjecicios_Click_1(object sender, EventArgs e)
        {
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            AbrirFormInPanel(new CapaPresentacion.InterfacesProfesor.Ejercicios());
            panelAvance.Visible = false;
        }

        private void btmInicio_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new CapaPresentacion.InterfacesProfesor.PIncio());
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            panelAvance.Visible = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btmInicio_Click(null, e);
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            panelAvance.Hide();
        }

        private void panelContenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btmAvanceGrupal_Click_1(object sender, EventArgs e)
        {
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            panelAvance.Visible = false;

            AbrirFormInPanel(new CapaPresentacion.InterfacesProfesor.AvanceGrupo());
            
        }

        private void btmAvanceIndividual_Click_1(object sender, EventArgs e)
        {
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            panelAvance.Visible = false;

            AbrirFormInPanel(new CapaPresentacion.InterfacesProfesor.AvanceIndividualA());
        }

        private void btmBaseDeDatos_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new CapaPresentacion.InterfacesProfesor.BaseDeDatos());
            btmLecciones.Location = new Point(0, 406);
            btmEjecicios.Location = new Point(0, 475);
            btmBaseDeDatos.Location = new Point(0, 539);
            panelAvance.Visible = false;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            IngresarProfesor ip = new IngresarProfesor();
            ip.Show();
            conexion.CerrarConexion();
            this.Close();
        }
    }
}
