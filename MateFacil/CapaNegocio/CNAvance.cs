﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using System.Data.SqlClient;
using System.Data;

namespace CapaNegocio
{
    public class CNAvance
    {
        CDAvances objAvance = new CDAvances();
        private String _BuscarAlumno;
        public String BuscarAlumno
        {
            set
            {
                if (value == "") { _BuscarAlumno = "Ingrese el nombre del alumno"; }
                else { _BuscarAlumno = value;}
            }
            get
            { return _BuscarAlumno; }
        }
        public SqlDataReader ConfirmarcAlumno()
        {
            SqlDataReader _ConfirmarAlumno;
            _ConfirmarAlumno = objAvance.ConfirmarNombre(BuscarAlumno);

            return _ConfirmarAlumno;
        }
        public DataTable MostrarAlumno(String nom,String curso)
        {
            DataTable tabla = new DataTable();
            tabla = objAvance.MostrarAlumno(nom,curso);
            return tabla;
        }
    }
}
