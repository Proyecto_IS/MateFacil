﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using System.Data.SqlClient;
using System.Data;

namespace CapaNegocio
{
   
    public class CNEjercicios
    {
        //Encapsular Variables 
        private CDEjercicios ObjDatoEjercicio = new CDEjercicios();

        //Variables 
        //Registro Ejercicio
        private String _IdEjercicios;
        private String _Planteamiento;
        private String _Respuesta;
        private String _LeccionEjercicio;

        private String _Leccion;
        private String _IDLeccion;

        //Metodo Get y Set --> Para el manejo de variables


        //Variables para Crear Leccion

       public String Leccion
        {
            set
            {
                if (value == "") { _Leccion = "Ingrese nombre de la lección"; }
                else { _Leccion = value; }
            }
            get { return _Leccion; }
        }
        public String IdLeccion
        {
            set
            {
                if (value == "") { _IDLeccion = "Error Conexión"; }
                else { _IDLeccion = value; }
            }
            get { return _IDLeccion; }
        }

        //Variables para Agreagr Ejercicio
        public String IdEjercicios
        {
            set
            {
                if (value == "") { _IdEjercicios = "Error conexión"; }
                else { _IdEjercicios = value; }
            }
            get { return _IdEjercicios; }
        }
        public String Planteamiento
        {
            set
            {
                if (value == "") { _Planteamiento = "Ingrese el planteamiento del Ejercicio"; }
                else { _Planteamiento = value; }
            }
            get { return _Planteamiento; }
        }
        public String LeccionEjercicio
        {
            set
            {
                if (value == "") { _LeccionEjercicio = "Elija Lección del Ejercicio"; }
                else { _LeccionEjercicio = value; }
            }
            get { return _LeccionEjercicio; }
        }
        public String Respuesta
        {
            set
            {
                if (value == "") { _Respuesta = "Ingrese respuesta del Ejercicio"; }
                else { _Respuesta = value; }
            }
            get { return _Respuesta; }
        }


        //FUNCIONES
        public SqlDataReader VerficacionDeLeccion()
        {
            SqlDataReader verificarLeccion;
            verificarLeccion = ObjDatoEjercicio.IdLeccionConfirmacion(LeccionEjercicio);

            return verificarLeccion;
        }

        public DataTable MostrarEjercicios(String cadena ,String curso)
        {
            DataTable tabla = new DataTable();
            tabla = ObjDatoEjercicio.Mostrar(cadena,curso);
            return tabla;
        }

        public DataTable MostrarEjerciciosAlumnos(String idalumno, String leccion)
        {
            DataTable tabla = new DataTable();
            tabla = ObjDatoEjercicio.MostrarParaAlumno(idalumno, leccion);
            return tabla;
        }
    }
}
