﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class CDEjercicios
    {
        CDConexion conexion = new CDConexion();
        private SqlDataReader ConfirmarLeccion;
        public bool RegistrarEjercicio(String id, String planteamiento, String respuesta, String leccion, String curso)
        {

            bool agregado = false;
            int i = 0;
            SqlCommand Registro = new SqlCommand("NuevoEjercicio '" + id + "','" + planteamiento + "','" + respuesta + "','" + leccion + "','"+curso+"'", conexion.AbrirConexion());
            i = Registro.ExecuteNonQuery();
           

            if (i > 0) agregado = true;

            conexion.CerrarConexion();

            return agregado;
        }

        public bool EliminarEjercicio(String dato)
        {
            bool Eliminar = false;
            int i = 0;
            SqlCommand EliminarE = new SqlCommand("delete from Ejercicios where Id_Ejercicios = " + "'" + dato + "'", conexion.AbrirConexion());
            i = EliminarE.ExecuteNonQuery();


            if (i > 0) Eliminar = true;

            conexion.CerrarConexion();

            return Eliminar;
        }

        public SqlDataReader IdLeccionConfirmacion(String codigo)
        {

            SqlCommand VerificacionIdLeccion = new SqlCommand("SPValidarLeccion", conexion.AbrirConexion());
            VerificacionIdLeccion.CommandType = CommandType.StoredProcedure;
            VerificacionIdLeccion.Parameters.AddWithValue("@leccion", codigo);

            ConfirmarLeccion = VerificacionIdLeccion.ExecuteReader();
            return ConfirmarLeccion;
        }

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();

        public DataTable Mostrar(String cadena, String curso)
        {
             comando = new SqlCommand("DesplegarEjercicios ", conexion.AbrirConexion());
             comando.CommandType = CommandType.StoredProcedure;
             comando.Parameters.AddWithValue("@nomb_Leccion", cadena);
             comando.Parameters.AddWithValue("@id_curso", curso);
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarParaAlumno(String idalumno, String idleccion)
        {
            comando = new SqlCommand("DesplegarEjerciciosAlumno ", conexion.AbrirConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@Id_Alumno", idalumno);
            comando.Parameters.AddWithValue("@Id_Leccion", idleccion);
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }
    }
}
