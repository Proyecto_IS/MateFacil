﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Net.Mail;
namespace CapaDatos
{
    public class CDUsuarios
    {
        private CDConexion Conexion = new CDConexion();
        private SqlDataReader leer;
        private SqlDataReader leer2;
        private SqlDataReader Confirmacion;
        private SqlDataReader ConfirmarEmail;
        private SqlDataReader confirmarUsuario;
        private String Email, Nombres, Contraseña;
        private String Mensaje;
        private SqlCommand Comando = new SqlCommand();
        CDConexion conexion = new CDConexion();

        public String GenerarCodigoCurso()
        {
            String codigo = "";
            SqlCommand comando = new SqlCommand("SELECT MAX(Id_Curso) FROM Curso", conexion.AbrirConexion());
            codigo = Convert.ToString(comando.ExecuteScalar());

            if (codigo.Equals(""))
            {
                codigo = "C0001";
            }
            else
            {
                String nuevo = "";

                for (int i = 1; i < 5; i++)
                {
                    nuevo += codigo[i];
                }

                int num = Convert.ToInt32(nuevo);
                num++;
                nuevo = Convert.ToString(num);
                if (nuevo.Length == 1)
                {
                    codigo = "C000" + nuevo;
                }
                else if (nuevo.Length == 2)
                {
                    codigo = "C00" + nuevo;
                }
                else if (nuevo.Length == 3)
                {
                    codigo = "C0" + nuevo;
                }
                else if (nuevo.Length == 4)
                {
                    codigo = "C" + nuevo;
                }


            }

            return codigo;
        }

        public String RecuperarContraseña(String dato)
        {
            confirmarUsuario.Close();
            Comando.Connection = Conexion.AbrirConexion();
            Comando.CommandText = "Select * from Usuario where NomUsuario='"+dato+"'";
            leer2 = Comando.ExecuteReader();
            if (leer2.Read() == true)
            {
                Email = leer2["Email"].ToString();
                Nombres = leer2["Nombres"].ToString() + "," + leer2["Apellido"].ToString();
                Contraseña = leer2["Contraseña"].ToString();
                //EMAIL
                EnviarEmail();

                Mensaje = "Estimado " + Nombres + ", Se ha enviado su Contraseña a su correo: " + Email + "\n Verifique su bandeja de entrada";
                leer2.Close();
            }
            else
            {
                Mensaje = "No se encontro datos....";
            }
            return Mensaje;
        }
        public void EnviarEmail()
        {
            //CORREO
            MailMessage correo = new MailMessage();
            correo.From = new MailAddress("argosmatefacil@gmail.com");
            correo.To.Add(Email);
            correo.Subject = ("RECUPERAR CONTRASEÑA SYSTEM");
            correo.Body = "Hola, " + Nombres + " Usted solicito recuperar contraseña\n Su contraseña es: " + Contraseña;
            correo.Priority = MailPriority.High;

            //SMPT
            SmtpClient ServerMail = new SmtpClient();
            ServerMail.Credentials = new NetworkCredential("argosmatefacil@gmail.com", "@admin123");
            ServerMail.Host = "smtp.gmail.com";
            ServerMail.Port = 587;
            ServerMail.EnableSsl = true;
            try
            {
                ServerMail.Send(correo);
            }
            catch(Exception ex)
            {
               
            }
            correo.Dispose();
        }
        //comentarios
        public SqlDataReader ConfirmarUsuario(String us)
        {
            SqlCommand comandoUsuario = new SqlCommand("SPConfirmarUsuario",Conexion.AbrirConexion());
            comandoUsuario.CommandType = CommandType.StoredProcedure;
            comandoUsuario.Parameters.AddWithValue("@Usuario", us);

            confirmarUsuario = comandoUsuario.ExecuteReader();
            return confirmarUsuario;
            
        }
        public SqlDataReader ConfirmarCorreo(String email)
        {
            confirmarUsuario.Close();
            SqlCommand comandoEmail = new SqlCommand("SPConfirmarCorreo", Conexion.AbrirConexion());
            comandoEmail.CommandType = CommandType.StoredProcedure;
            comandoEmail.Parameters.AddWithValue("@Correo", email);

            ConfirmarEmail = comandoEmail.ExecuteReader();
            return ConfirmarEmail;
        }
        public SqlDataReader IniciarSesion(String user, String pass) {
        
            SqlCommand comando = new SqlCommand("SPIniniarSesion",Conexion.AbrirConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@Usuario",user);
            comando.Parameters.AddWithValue("@Password",pass);

            leer = comando.ExecuteReader();
            return leer;
        }
        public SqlDataReader CodigoConfirmacion(String codigo) {
            SqlCommand comandoVeri = new SqlCommand("SPCodigo", Conexion.AbrirConexion());
            comandoVeri.CommandType = CommandType.StoredProcedure;
            comandoVeri.Parameters.AddWithValue("@CodigoVeri",codigo);

            Confirmacion = comandoVeri.ExecuteReader();
            return Confirmacion;
        }
        public bool RegistrarMaestro(String NomUsuario, String Password, String Nombre, String Apellido, String Correo,String Id)
        {
            bool agregado = false;
            int i = 0;
            String idc = GenerarCodigoCurso();

            SqlCommand Registro = new SqlCommand("insert into Usuario values('"+NomUsuario+"','"+Password+"','"+Nombre+"','"+Apellido+"','"+Correo+"')",Conexion.AbrirConexion());
            i = Registro.ExecuteNonQuery();

            i = 0;
            SqlCommand Registro2 = new SqlCommand("insert into Profesor values('" + Id + "','" + Nombre + "',"+1+")", Conexion.AbrirConexion());
            i = Registro2.ExecuteNonQuery();

            i = 0;
            SqlCommand Registro3 = new SqlCommand("insert into Curso values('" + idc + "','" + Nombre + "','"+Id+"')", Conexion.AbrirConexion());
            i = Registro3.ExecuteNonQuery();


            if (i > 0) agregado = true;

            Conexion.CerrarConexion();

            return agregado;
        }
    }
}
