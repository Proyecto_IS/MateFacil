﻿using System;
using System.Data.SqlClient;
using System.Data;


namespace CapaDatos
{
    public class CDAlumno
    {
        CDConexion conexion = new CDConexion();
        private SqlDataReader ConfirmarAlumno;

       

        public bool RegistrarAlumno(String id, String nombre, String grado, String grupo,String curso,String cal)
        {


            bool agregado = false;
            int i = 0;
            SqlCommand Registro = new SqlCommand("NuevoAlumno '" + id + "','" + nombre + "','" + grado + "','" + grupo + "','"+curso+"'", conexion.AbrirConexion());
            i = Registro.ExecuteNonQuery();
            i = 0;
            SqlCommand Registro2 = new SqlCommand("insert into Usuario values('" + id + "','"+id+"',null,null,null)", conexion.AbrirConexion());
            i = Registro2.ExecuteNonQuery();
            i = 0;
            SqlCommand Registro3 = new SqlCommand("insert into Calificaciones values('" + cal + "','"+id+"',"+0+",null)", conexion.AbrirConexion());
            i = Registro3.ExecuteNonQuery();

            if (i > 0) agregado = true;

            conexion.CerrarConexion();

            return agregado;
        }

        public bool EliminarAlunmo(String dato, String dato2)
        {
            bool Eliminar = false;
            int i = 0;
            SqlCommand EliminarAA = new SqlCommand("BorrarAvance '" + dato + "'", conexion.AbrirConexion());
            i = EliminarAA.ExecuteNonQuery();
            i = 0;
            SqlCommand EliminarC = new SqlCommand("BorrarCal '" + dato + "'", conexion.AbrirConexion());
            i = EliminarC.ExecuteNonQuery();
            i = 0;
            SqlCommand EliminarA = new SqlCommand("delete from Alumno where Nombre_Alumno = " + "'" + dato + "'", conexion.AbrirConexion());
            i = EliminarA.ExecuteNonQuery();
            i = 0;
            SqlCommand EliminarU = new SqlCommand("delete from Usuario where NomUsuario =" + "'" + dato2 + "'", conexion.AbrirConexion());
            i = EliminarU.ExecuteNonQuery();
            
            
            if (i > 0) Eliminar = true;

            conexion.CerrarConexion();

            return Eliminar;
        }

        public bool ModificarAlumno(String id, String nombre, String grado, String grupo)
        {

            bool modi = false;
            int i = 0;
            SqlCommand Modificar = new SqlCommand("ModificarAlumno  '" + id + "','" + nombre + "','" + grado + "','" + grupo + "'", conexion.AbrirConexion());
            i = Modificar.ExecuteNonQuery();

            if (i > 0) modi = true;

            conexion.CerrarConexion();

            return modi;
        }
        public SqlDataReader NombreDelAlumno(String dato)
        {
            SqlCommand VerificacionNombreAlumno = new SqlCommand("SPConfirmarAlumno", conexion.AbrirConexion());
            VerificacionNombreAlumno.CommandType = CommandType.StoredProcedure;
            VerificacionNombreAlumno.Parameters.AddWithValue("@Alumno", dato);

            ConfirmarAlumno = VerificacionNombreAlumno.ExecuteReader();
            return ConfirmarAlumno;
        }

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();

        public DataTable Mostrar(String curso)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "DesplegarAlumnos '"+curso+"'";
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }
    }
}
