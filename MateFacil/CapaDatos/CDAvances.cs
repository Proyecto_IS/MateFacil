﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaDatos;

namespace CapaDatos
{
    public class CDAvances
    {
        CDConexion conexion = new CDConexion();
        SqlDataReader _ConfirmarNombre;
        SqlDataReader leer;
        SqlCommand commando = new SqlCommand();
        DataTable tabla = new DataTable();
        public SqlDataReader ConfirmarNombre(String nom)
        {
            SqlCommand Nombre = new SqlCommand("SPConfirmarAlumno",conexion.AbrirConexion());
            Nombre.CommandType = CommandType.StoredProcedure;
            Nombre.Parameters.AddWithValue("@Alumno",nom);
            _ConfirmarNombre = Nombre.ExecuteReader();

            return _ConfirmarNombre;
        }
        public DataTable MostrarAlumno(String nombre, String curso)
        {
            commando = new SqlCommand("AvanceIndividual", conexion.AbrirConexion());
            commando.CommandType = CommandType.StoredProcedure;
            commando.Parameters.AddWithValue("@Alumno",nombre);
            commando.Parameters.AddWithValue("@id_curso", curso);
            leer = commando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();

            return tabla;
        }
    }
}
